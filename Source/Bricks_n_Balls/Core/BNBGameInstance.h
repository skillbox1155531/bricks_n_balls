// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "BNBGameInstance.generated.h"

class UBNBSaveGame;

UCLASS()
class BRICKS_N_BALLS_API UBNBGameInstance : public UGameInstance
{
	GENERATED_BODY()

public:
	void SaveGameData(const int32 Points);
	UBNBSaveGame* LoadGameData() const;

	FORCEINLINE bool GetIsContinueGame() const { return bIsContinueGame; }
	FORCEINLINE bool GetIsNewGame() const { return bIsNewGame; }
	FORCEINLINE bool GetIsAutoPlay() const { return bIsAutoPlay; }
	FORCEINLINE int32 GetCurrentPoints() const { return CurrentPoints; }
	FORCEINLINE int32 GetCurrentXRows() const { return CurrentXRows; }
	FORCEINLINE int32 GetCurrentYColumns() const { return CurrentYColumns; }
	
	FORCEINLINE void SetCurrentPoints(const float& _CurrentPoints) { CurrentPoints = _CurrentPoints; }
	FORCEINLINE void SetCurrentXRows(const float& _CurrentXRows) { CurrentXRows = _CurrentXRows; }
	FORCEINLINE void SetCurrentYColumns(const float& _CurrentYColumns) { CurrentYColumns = _CurrentYColumns; }
	FORCEINLINE void SetIsContinueGame(const bool& _bIsContinueGame) { bIsContinueGame = _bIsContinueGame; }
	FORCEINLINE void SetIsNewGame(const bool& _bIsNewGame) { bIsNewGame = _bIsNewGame; } 
	FORCEINLINE void SetIsAutoPlay(const bool& _bIsAutoPlay) { bIsAutoPlay = _bIsAutoPlay; } 
protected:
	virtual void Init() override;
	virtual void Shutdown() override;
	
private:
	UPROPERTY()
	UBNBSaveGame* GameData;
	const FString SaveSlot = "Game Data";
	UPROPERTY()
	int32 CurrentPoints;
	UPROPERTY()
	int32 CurrentXRows;
	UPROPERTY()
	int32 CurrentYColumns;
	UPROPERTY()
	bool bIsContinueGame;
	UPROPERTY()
	bool bIsNewGame;
	UPROPERTY()
	bool bIsAutoPlay;
};
