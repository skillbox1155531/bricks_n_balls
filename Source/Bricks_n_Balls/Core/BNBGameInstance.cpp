// Fill out your copyright notice in the Description page of Project Settings.

#include "BNBGameInstance.h"
#include "BNBSaveGame.h"
#include "Bricks_n_Balls/Player/BNBPlayerController.h"
#include "Kismet/GameplayStatics.h"

void UBNBGameInstance::Init()
{
	Super::Init();
	
	GameData = Cast<UBNBSaveGame>(UGameplayStatics::LoadGameFromSlot(SaveSlot, 0));

	if (!GameData)
	{
		GameData = Cast<UBNBSaveGame>(UGameplayStatics::CreateSaveGameObject(UBNBSaveGame::StaticClass()));
		GameData -> SetRecordPoints(0);

		UGameplayStatics::SaveGameToSlot(GameData, SaveSlot, 0);
	}
}

void UBNBGameInstance::Shutdown()
{
	Super::Shutdown();

	const auto* PlayerController = Cast<ABNBPlayerController>(GetPrimaryPlayerController());
	
	if (IsValid(PlayerController))
	{
		SaveGameData(PlayerController -> GetRecordPoints());
	}
}

void UBNBGameInstance::SaveGameData(const int32 Points)
{
	if (GameData)
	{
		GameData -> SetRecordPoints(Points);
		
		UGameplayStatics::SaveGameToSlot(GameData, SaveSlot, 0);
	}
}

UBNBSaveGame* UBNBGameInstance::LoadGameData() const
{
	return GameData ? GameData : nullptr;
}