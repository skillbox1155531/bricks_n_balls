// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/SaveGame.h"
#include "BNBSaveGame.generated.h"

UCLASS()
class BRICKS_N_BALLS_API UBNBSaveGame : public USaveGame
{
	GENERATED_BODY()

public:
	FORCEINLINE int32 GetRecordPoints() const { return RecordPoints; }
	FORCEINLINE void SetRecordPoints(const int32 _RecordPoints) { RecordPoints = _RecordPoints; }
private:
	UPROPERTY()
	int32 RecordPoints;
};
