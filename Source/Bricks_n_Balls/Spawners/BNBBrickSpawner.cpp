// Fill out your copyright notice in the Description page of Project Settings.

#include "BNBBrickSpawner.h"

#include "Bricks_n_Balls/Effects/BNBEffectBase.h"
#include "Kismet/KismetMathLibrary.h"
#include "Bricks_n_Balls/Obstacles/BNBBrickObstacleBase.h"
#include "Bricks_n_Balls/Bricks/BNBBrickBase.h"
#include "Bricks_n_Balls/Bricks/BNBNormalBrick.h"
#include "Bricks_n_Balls/Bricks/BNBMediumBrick.h"
#include "Bricks_n_Balls/Bricks/BNBHardBrick.h"
#include "Bricks_n_Balls/Core/BNBGameInstance.h"
#include "Bricks_n_Balls/Player/BNBPlayerController.h"
#include "Components/ArrowComponent.h"

// Sets default values
ABNBBrickSpawner::ABNBBrickSpawner()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	
	Parent = CreateDefaultSubobject<USceneComponent>("Parent");
	RootComponent = Parent;
	
	ObstacleStartPoint_1 = CreateDefaultSubobject<UArrowComponent>("Start Point 1");
	ObstacleStartPoint_1 -> AttachToComponent(RootComponent, FAttachmentTransformRules::KeepRelativeTransform);
	
	ObstacleEndPoint_1 = CreateDefaultSubobject<UArrowComponent>("End Point 1");
	ObstacleEndPoint_1 -> AttachToComponent(RootComponent, FAttachmentTransformRules::KeepRelativeTransform);

	ObstacleStartPoint_2 = CreateDefaultSubobject<UArrowComponent>("Start Point 2");
	ObstacleStartPoint_2 -> AttachToComponent(RootComponent, FAttachmentTransformRules::KeepRelativeTransform);
	
	ObstacleEndPoint_2 = CreateDefaultSubobject<UArrowComponent>("End Point 2");
	ObstacleEndPoint_2 -> AttachToComponent(RootComponent, FAttachmentTransformRules::KeepRelativeTransform);

	ObstacleStartPoint_3 = CreateDefaultSubobject<UArrowComponent>("Start Point 3");
	ObstacleStartPoint_3 -> AttachToComponent(RootComponent, FAttachmentTransformRules::KeepRelativeTransform);
	
	ObstacleEndPoint_3 = CreateDefaultSubobject<UArrowComponent>("End Point 3");
	ObstacleEndPoint_3 -> AttachToComponent(RootComponent, FAttachmentTransformRules::KeepRelativeTransform);

	PlayerController = nullptr;
	
	MaxXRows = 10;
	MinXRows = 3;
	XRows = MinXRows;
	
	MaxYColumns = 20;
	MinYColumns = 5;
	YColumns = MinYColumns;
	
	Padding = 120;
	YOffset = 50;

	BricksAmount = 0;
	EffectSpawnChance = 0.f;

	MediumBrickPointsThreshold = 200;
	HardBrickPointsThreshold = 500;

	OneObstaclePointsThreshold = 1000;
	TwoObstaclesPointsThreshold = 1500;
	ThreeObstaclesPointsThreshold = 2000;
}

void ABNBBrickSpawner::Initialization(ABNBPlayerController* _PlayerController)
{
	if (_PlayerController)
	{
		PlayerController = _PlayerController;
		InitializeBrickArray(0);
		SpawnGrid(FMath::RandRange(0, 2), 0);
	}
}

void ABNBBrickSpawner::Initialization(ABNBPlayerController* _PlayerController, const UBNBGameInstance* GameInstance,
                                      const int32& CurrentPoints)
{
	if (IsValid(_PlayerController), IsValid(GameInstance))
	{
		PlayerController = _PlayerController;

		XRows = CalculateGridValues(GameInstance -> GetCurrentXRows() , MaxXRows);
		YColumns = CalculateGridValues(GameInstance -> GetCurrentYColumns(), MaxYColumns);

		InitializeBrickArray(CurrentPoints);

		SpawnGrid(FMath::RandRange(0, 2), CurrentPoints);
	}
}

void ABNBBrickSpawner::InitializeBrickArray(const int32& CurrentPoints)
{
	BricksArr.Emplace(NormalBrickClass);
	
	if (CurrentPoints >= MediumBrickPointsThreshold)
	{
		BricksArr.Emplace(MediumBrickClass);
	}
	if (CurrentPoints >= HardBrickPointsThreshold)
	{
		BricksArr.Emplace(HardBrickClass);
	}
}

int32 ABNBBrickSpawner::CalculateGridValues(const int32& CurrentValue, const int32& MaxValue)
{
	if (CurrentValue >= MaxValue)
	{
		return MaxValue;
	}
	
	return CurrentValue + 1;
}

void ABNBBrickSpawner::CallEnableCollision(const bool& Value)
{
	OnEnableBrickCollision.Broadcast(Value);
}

void ABNBBrickSpawner::CallDeactivateEffects()
{
	OnDeactivateEffects.Broadcast();
}

void ABNBBrickSpawner::BrickDestroyed(int32 Points)
{
	if (BricksAmount != 0)
	{
		BricksAmount--;

		if (IsValid(PlayerController))
		{
			PlayerController -> IncreasePoints(Points);

			if (BricksAmount == 0)
			{
				CallDeactivateEffects();
				PlayerController -> HandleLevelCompleted(false);
			}
		}
	}
}

void ABNBBrickSpawner::SpawnGrid(const int32& Value, const int32& CurrentPoints)
{
	if (Value == 0)
	{
		SpawnCrossPattern(XRows, CurrentPoints);
	}
	else if(Value == 1)
	{
		SpawnChessPattern(XRows, YColumns, CurrentPoints);
	}
	else
	{
		SpawnClassicPattern(XRows, YColumns, CurrentPoints);	
	}
}

void ABNBBrickSpawner::SpawnChessPattern(const int32& Rows, const int32& Columns, const int32& CurrentPoints)
{
	SetSpawnerLocation(Rows, Columns);
	
	bool bIsOdd = false;
	
	for (int i = 0; i < Rows; ++i)
	{
		for (int j = bIsOdd; j < Columns; j += 2)
		{
			SpawnBrick(i, j);
			BricksAmount++;
		}

		bIsOdd = !bIsOdd;
	}
	
	CalculateObstaclesNumber(CurrentPoints);
}

void ABNBBrickSpawner::SpawnCrossPattern(const int32& Rows, const int32& CurrentPoints)
{
	SetSpawnerLocation(Rows);
	
	for (int i = 0; i < Rows; ++i)
	{
		for (int j = 0; j < Rows; ++j)
		{
			if (j == i || j == i + 1 || (j + i == Rows - 1) || (j + (i + 1) == Rows - 1))
			{
				SpawnBrick(i, j);
				BricksAmount++;
			}
		}
	}
	
	CalculateObstaclesNumber(CurrentPoints);
}

void ABNBBrickSpawner::SpawnClassicPattern(const int32& Rows, const int32& Columns, const int32& CurrentPoints)
{
	SetSpawnerLocation(Rows, Columns);
	
	for (int i = 0; i < Rows; ++i)
	{
		for (int j = 0; j < Columns; ++j)
		{
			SpawnBrick(i, j);
			BricksAmount++;
		}
	}

	CalculateObstaclesNumber(CurrentPoints);
}

void ABNBBrickSpawner::SetSpawnerLocation(const int32& Rows)
{
	FVector SpawnerLocation = GetActorLocation();

	const float SpawnerOffsetX = Padding * (MaxXRows - Rows);
	const float SpawnerOffsetY = YOffset * (Rows + 1.5);

	SpawnerLocation = FVector(SpawnerLocation.X + SpawnerOffsetX, SpawnerLocation.Y - SpawnerOffsetY, SpawnerLocation.Z);
	SetActorLocation(SpawnerLocation);

	SetObstaclePointsLocation(SpawnerOffsetY);
}

void ABNBBrickSpawner::SetSpawnerLocation(const int32& Rows, const int32& Columns)
{
	FVector SpawnerLocation = GetActorLocation();
	
	const float SpawnerOffsetX = Padding * (MaxXRows - Rows);
	const float SpawnerOffsetY = YOffset * (Columns + 2.5);
	
	SpawnerLocation = FVector(SpawnerLocation.X + SpawnerOffsetX, SpawnerLocation.Y - SpawnerOffsetY, SpawnerLocation.Z);
	SetActorLocation(SpawnerLocation);

	SetObstaclePointsLocation(SpawnerOffsetY);
}

void ABNBBrickSpawner::SetObstaclePointsLocation(const int32& Offset)
{
	SetObstaclePointLocation(ObstacleStartPoint_1, Offset);
	SetObstaclePointLocation(ObstacleEndPoint_1, Offset);
	SetObstaclePointLocation(ObstacleStartPoint_2, Offset);
	SetObstaclePointLocation(ObstacleEndPoint_2, Offset);
	SetObstaclePointLocation(ObstacleStartPoint_3, Offset);
	SetObstaclePointLocation(ObstacleEndPoint_3, Offset);
}

void ABNBBrickSpawner::SetObstaclePointLocation(UArrowComponent* Component, const int32& Offset)
{
	if (Component)
	{
		const FVector Location = Component -> GetComponentLocation();
		const FVector NewLocation(Location.X, Location.Y + Offset, Location.Z);
		
		Component -> SetWorldLocation(NewLocation);
	}
}


void ABNBBrickSpawner::SpawnBrick(const int32& XIndex, const int32& YIndex)
{
	if (IsValid(GetWorld()))
	{
		const float XLocation = XIndex * Padding;
		const float YLocation = YIndex * Padding;

		const FVector Location(XLocation, YLocation, 0.f);
		const FRotator Rotation = FQuat::Identity.Rotator();

		FActorSpawnParameters SpawnParameters;
		SpawnParameters.Owner = this;

		const auto BrickToSpawn = GetRandomBrick();

		if (IsValid(BrickToSpawn))
		{
			auto* Brick = GetWorld() -> SpawnActor<ABNBBrickBase>(BrickToSpawn, Location, Rotation, SpawnParameters);
			Brick -> AttachToActor(this, FAttachmentTransformRules::KeepRelativeTransform);
			Brick -> SetBrickSpawner(this);
			
			OnEnableBrickCollision.AddUObject(Brick, &ABNBBrickBase::EnableCollision);
			
			SpawnEffect(Brick, Rotation, SpawnParameters);
		}
	}
}

void ABNBBrickSpawner::SpawnEffect(ABNBBrickBase* Brick, const FRotator Rotation, FActorSpawnParameters SpawnParameters)
{
	const auto bIsSpawnEffect =  UKismetMathLibrary::RandomBoolWithWeight(EffectSpawnChance);

	if (bIsSpawnEffect)
	{
		const auto EffectToSpawn = GetRandomEffect();
		auto* Effect = GetWorld() -> SpawnActor<ABNBEffectBase>(EffectToSpawn, Brick -> GetActorLocation(), Rotation, SpawnParameters);

		if (IsValid(Effect))
		{
			Effect -> AttachToActor(Brick, FAttachmentTransformRules::KeepWorldTransform);
			Effect -> SetActive(Effect, false);
			OnDeactivateEffects.AddUObject(Effect, &ABNBEffectBase::Deactivate);

			Brick -> SetEffectPtr(Effect);
		}
	}
}

void ABNBBrickSpawner::CalculateObstaclesNumber(const int32& CurrentPoints)
{
	if (IsValid(ObstacleStartPoint_1) && IsValid(ObstacleEndPoint_1) && IsValid(ObstacleStartPoint_2) && IsValid(ObstacleEndPoint_2))
	{
		int32 ObstacleNumber = 0;
		
		if (CurrentPoints >= OneObstaclePointsThreshold)
		{
			ObstacleNumber += 1;
		}
		if (CurrentPoints >= TwoObstaclesPointsThreshold)
		{
			ObstacleNumber += 1;
		}
		if (CurrentPoints >= ThreeObstaclesPointsThreshold)
		{
			ObstacleNumber += 1;
		}

		SpawnObstacles(ObstacleNumber);
	}
}

void ABNBBrickSpawner::SpawnObstacles(int32 ObstacleNumber)
{
	if (ObstacleNumber == 1)
	{
		SpawnObstacle(ObstacleEndPoint_2->GetComponentLocation(), ObstacleStartPoint_1 -> GetComponentLocation());
	}
	else if (ObstacleNumber == 2)
	{
		SpawnObstacle(ObstacleStartPoint_1->GetComponentLocation(), ObstacleEndPoint_1 -> GetComponentLocation());
		SpawnObstacle(ObstacleEndPoint_2->GetComponentLocation(), ObstacleStartPoint_2 -> GetComponentLocation());
	}
	else if (ObstacleNumber == 3)
	{
		SpawnObstacle(ObstacleStartPoint_1->GetComponentLocation(), ObstacleEndPoint_1 -> GetComponentLocation());
		SpawnObstacle(ObstacleEndPoint_2->GetComponentLocation(), ObstacleStartPoint_2 -> GetComponentLocation());
		SpawnObstacle(ObstacleStartPoint_3->GetComponentLocation(), ObstacleEndPoint_3 -> GetComponentLocation());
	}
}

void ABNBBrickSpawner::SpawnObstacle(const FVector& StartPoint, const FVector& EndPoint)
{
	const FRotator Rotation = FQuat::Identity.Rotator();

	FActorSpawnParameters SpawnParameters;
	SpawnParameters.Owner = this;

	const auto ObstacleToSpawn = GetRandomObstacle();

	if (IsValid(ObstacleToSpawn))
	{
		auto* Obstacle = GetWorld() -> SpawnActor<ABNBBrickObstacleBase>(ObstacleToSpawn, StartPoint, Rotation, SpawnParameters);
		Obstacle -> AttachToActor(this, FAttachmentTransformRules::KeepWorldTransform);

		Obstacle -> SetStartPoint(StartPoint);
		Obstacle -> SetEndPoint(EndPoint);
		Obstacle -> SetTargetPoint(EndPoint);
	}	
}

TSubclassOf<ABNBBrickBase> ABNBBrickSpawner::GetRandomBrick()
{
	if (BricksArr.Num() != 0)
	{
		const int32 Min = 0;
		const int32 Max = BricksArr.Num() - 1;
		const int32 Index = FMath::RandRange(Min, Max);
		
		return BricksArr[Index];
	}
	
	return nullptr;
}

TSubclassOf<ABNBBrickObstacleBase> ABNBBrickSpawner::GetRandomObstacle()
{
	if (ObstacleArr.Num() != 0)
	{
		const int32 Min = 0;
		const int32 Max = ObstacleArr.Num() - 1;
		const int32 Index = FMath::RandRange(Min, Max);
		
		return ObstacleArr[Index];
	}

	return nullptr;
}

TSubclassOf<ABNBEffectBase> ABNBBrickSpawner::GetRandomEffect()
{
	if (EffectsArr.Num() != 0)
	{
		const int32 Min = 0;
		const int32 Max = EffectsArr.Num() - 1;
		const int32 Index = FMath::RandRange(Min, Max);
		
		return EffectsArr[Index];
	}

	return nullptr;
}
