// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Bricks_n_Balls/Bricks/BNBBrickBase.h"
#include "Bricks_n_Balls/Helper/DelegatesHolder.h"
#include "GameFramework/Actor.h"
#include "BNBBrickSpawner.generated.h"

class ABNBHardBrick;
class ABNBMediumBrick;
class ABNBNormalBrick;
class UBNBGameInstance;
class ABNBPlayerController;
class ABNBBrickBase;
class ABNBBrickObstacleBase;
class ABNBEffectBase;
class UArrowComponent;
class USceneComponent;

UCLASS()
class BRICKS_N_BALLS_API ABNBBrickSpawner : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABNBBrickSpawner();

private:
	void SpawnGrid(const int32& Value, const int32& CurrentPoints);
	void SpawnChessPattern(const int32& Rows, const int32& Columns, const int32& CurrentPoints);
	void SpawnCrossPattern(const int32& Rows, const int32& CurrentPoints);
	void SpawnClassicPattern(const int32& Rows, const int32& Columns, const int32& CurrentPoints);
	void SetSpawnerLocation(const int32& Rows);
	void SetSpawnerLocation(const int32& Rows, const int32& Columns);
	void SetObstaclePointsLocation(const int32& Offset);
	void SetObstaclePointLocation(UArrowComponent* Component, const int32& Offset);
	void SpawnEffect(ABNBBrickBase* Brick, FRotator Rotation, FActorSpawnParameters SpawnParameters);
	void SpawnObstacles(int32 ObstacleNumber);
	void SpawnBrick(const int32& XIndex, const int32& YIndex);
	void CalculateObstaclesNumber(const int32& CurrentPoints);
	void SpawnObstacle(const FVector& StartPoint, const FVector& EndPoint);
	TSubclassOf<ABNBBrickBase> GetRandomBrick();
	TSubclassOf<ABNBBrickObstacleBase> GetRandomObstacle();
	TSubclassOf<ABNBEffectBase> GetRandomEffect();
	
protected:

public:
	void Initialization(ABNBPlayerController* _PlayerController);
	void Initialization(ABNBPlayerController* _PlayerController, const UBNBGameInstance* GameInstance, const int32& CurrentPoints);
	void InitializeBrickArray(const int32& CurrentPoints);
	int32 CalculateGridValues(const int32& CurrentValue, const int32& MaxValue);
	void CallEnableCollision(const bool& Value);
	void CallDeactivateEffects();
	void BrickDestroyed(int32 Points);

	FORCEINLINE int32 GetXRows() const { return XRows; }
	FORCEINLINE int32 GetMinXRows() const { return MinXRows; }
	FORCEINLINE int32 GetYColumns() const { return YColumns; }
	FORCEINLINE int32 GetMinYColumns() const { return MinYColumns; }
	
	FORCEINLINE void SetXRows(const int32& Value) { XRows = Value; }
	FORCEINLINE void SetYColumns(const int32& Value) { YColumns = Value; }
private:
	UPROPERTY()
	ABNBPlayerController* PlayerController;
	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<ABNBNormalBrick> NormalBrickClass;
	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<ABNBMediumBrick> MediumBrickClass;
	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<ABNBHardBrick> HardBrickClass;
	UPROPERTY(VisibleAnywhere, Category="Spawner | Objects")
	TArray<TSubclassOf<ABNBBrickBase>> BricksArr;
	UPROPERTY(EditDefaultsOnly, Category="Spawner | Objects")
	TArray<TSubclassOf<ABNBBrickObstacleBase>> ObstacleArr;
	UPROPERTY(EditDefaultsOnly, Category="Spawner | Objects")
	TArray<TSubclassOf<ABNBEffectBase>> EffectsArr;
	UPROPERTY(VisibleAnywhere, Category="Spawner | Grid Settings")
	int32 XRows;
	UPROPERTY(EditDefaultsOnly, Category="Spawner | Grid Settings")
	int32 MaxXRows;
	UPROPERTY(EditDefaultsOnly, Category="Spawner | Grid Settings")
	int32 MinXRows;
	UPROPERTY(VisibleAnywhere, Category="Spawner | Grid Settings")
	int32 YColumns;
	UPROPERTY(EditDefaultsOnly, Category="Spawner | Grid Settings")
	int32 MaxYColumns;
	UPROPERTY(EditDefaultsOnly, Category="Spawner | Grid Settings")
	int32 MinYColumns;
	UPROPERTY(EditDefaultsOnly, Category="Spawner | Grid Settings")
	int32 Padding;
	UPROPERTY(EditDefaultsOnly, Category="Spawner | Grid Settings")
	int32 YOffset;
	UPROPERTY(VisibleAnywhere, Category="Spawner | Grid Settings")
	int32 BricksAmount;
	UPROPERTY(EditDefaultsOnly, Category="Spawner | Grid Settings")
	int32 OneObstaclePointsThreshold;
	UPROPERTY(EditDefaultsOnly, Category="Spawner | Grid Settings")
	int32 TwoObstaclesPointsThreshold;
	UPROPERTY(EditDefaultsOnly, Category="Spawner | Grid Settings")
	int32 ThreeObstaclesPointsThreshold;
	UPROPERTY(EditDefaultsOnly, Category="Spawner | Grid Settings")
	int32 MediumBrickPointsThreshold;
	UPROPERTY(EditDefaultsOnly, Category="Spawner | Grid Settings")
	int32 HardBrickPointsThreshold;
	UPROPERTY(EditDefaultsOnly, Category="Spawner | Grid Settings")
	float EffectSpawnChance;

#pragma region ObstaclePoints
	UPROPERTY(EditDefaultsOnly)
	USceneComponent* Parent;
	UPROPERTY(EditDefaultsOnly)
	UArrowComponent* ObstacleStartPoint_1;
	UPROPERTY(EditDefaultsOnly)
	UArrowComponent* ObstacleEndPoint_1;
	UPROPERTY(EditDefaultsOnly)
	UArrowComponent* ObstacleStartPoint_2;
	UPROPERTY(EditDefaultsOnly)
	UArrowComponent* ObstacleEndPoint_2;
	UPROPERTY(EditDefaultsOnly)
	UArrowComponent* ObstacleStartPoint_3;
	UPROPERTY(EditDefaultsOnly)
	UArrowComponent* ObstacleEndPoint_3;
#pragma endregion

	FOnEnableBrickCollision OnEnableBrickCollision;
	FOnDeactivateEffects OnDeactivateEffects;
protected:
};
