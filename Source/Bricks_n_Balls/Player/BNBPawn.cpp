// Fill out your copyright notice in the Description page of Project Settings.


#include "BNBPawn.h"

#include "BNBPlayerController.h"
#include "NiagaraComponent.h"
#include "NiagaraFunctionLibrary.h"
#include "Components/ArrowComponent.h"
#include "Components/CapsuleComponent.h"

// Sets default values
ABNBPawn::ABNBPawn()
{
	PrimaryActorTick.bCanEverTick = false;

	CapsuleComponent = CreateDefaultSubobject<UCapsuleComponent>("Collision");
	CapsuleComponent -> SetWorldRotation(FRotator(90.f, 90.f, 0.f));
	CapsuleComponent -> SetCapsuleHalfHeight(140.f);
	CapsuleComponent -> SetCapsuleRadius(30.f);
	RootComponent = CapsuleComponent;
	
	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>("Mesh");
	MeshComponent -> AttachToComponent(RootComponent, FAttachmentTransformRules::KeepRelativeTransform);
	MeshComponent -> SetCollisionEnabled(ECollisionEnabled::NoCollision);
	MeshComponent -> SetWorldScale3D(FVector(2.5f, 0.2f, 0.5f));
	MeshComponent -> AttachToComponent(RootComponent, FAttachmentTransformRules::KeepRelativeTransform);

	ArrowComponent = CreateDefaultSubobject<UArrowComponent>("Arrow");
	ArrowComponent -> AttachToComponent(MeshComponent, FAttachmentTransformRules::KeepRelativeTransform);

	LeftTrailPoint = CreateDefaultSubobject<UArrowComponent>("Left Trail");
	LeftTrailPoint -> AttachToComponent(MeshComponent, FAttachmentTransformRules::KeepRelativeTransform);

	RightTrailPoint = CreateDefaultSubobject<UArrowComponent>("Right Trail");
	RightTrailPoint -> AttachToComponent(MeshComponent, FAttachmentTransformRules::KeepRelativeTransform);

	BasePawnSpeed = 2500.f;
	CurrentPawnSpeed = BasePawnSpeed;
}

// Called when the game starts or when spawned
void ABNBPawn::BeginPlay()
{
	Super::BeginPlay();
}

void ABNBPawn::Initialize(ABNBPlayerController* _PlayerController)
{
	if (_PlayerController)
	{
		PlayerController = _PlayerController;
	}

	LeftTrailEffect = UNiagaraFunctionLibrary::SpawnSystemAttached(TrailEffect, LeftTrailPoint, NAME_None,
		LeftTrailPoint->GetComponentLocation(), LeftTrailPoint->GetComponentRotation(), EAttachLocation::KeepWorldPosition, false);
	if(LeftTrailEffect) LeftTrailEffect -> SetVisibility(false);
	
	RightTrailEffect = UNiagaraFunctionLibrary::SpawnSystemAttached(TrailEffect, RightTrailPoint, NAME_None,
			RightTrailPoint->GetComponentLocation(), RightTrailPoint->GetComponentRotation(), EAttachLocation::KeepWorldPosition, false);
	if(RightTrailEffect) RightTrailEffect -> SetVisibility(false);
}

void ABNBPawn::CallApplyEffect(const FBNBEffectData& EffectData) const
{
	if (PlayerController)
	{
		PlayerController -> OnApplyEffect.ExecuteIfBound(EffectData);
	}
}
