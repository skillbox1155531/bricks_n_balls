// Fill out your copyright notice in the Description page of Project Settings.

#include "BNBPlayerController.h"
#include "BNBPawn.h"
#include "NiagaraComponent.h"
#include "Bricks_n_Balls/Player/Componenets/BNBEffectHandlerComponent.h"
#include "Bricks_n_Balls/Player/Componenets/BNBInputHandlerComponent.h"
#include "Bricks_n_Balls/Player/Componenets/BNBGameStateHandlerComponent.h"
#include "Bricks_n_Balls/UI/WBNBEndLevelScreen.h"
#include "Bricks_n_Balls/UI/WBNBGameMenu.h"
#include "Bricks_n_Balls/UI/WBNBMainMenu.h"
#include "Bricks_n_Balls/UI/WBNBPlayerUI.h"
#include "Bricks_n_Balls/Ball/BNBBonusBall.h"
#include "Bricks_n_Balls/Ball/BNBMainBall.h"
#include "Bricks_n_Balls/Core/BNBGameInstance.h"
#include "Bricks_n_Balls/Spawners/BNBBrickSpawner.h"
#include "GameFramework/GameUserSettings.h"
#include "Kismet/GameplayStatics.h"

ABNBPlayerController::ABNBPlayerController()
{
	PrimaryActorTick.bCanEverTick = true;

	PlayerPawn = nullptr;
	MainBallPtr = nullptr;
	BrickSpawner = nullptr;

	CurrentPoints = 0;
	RecordPoints = 0;

	MaxLives = 5;
	CurrentLives = 3;

	InputHandlerComponent = CreateDefaultSubobject<UBNBInputHandlerComponent>(TEXT("Input Handler Component"));
	AddOwnedComponent(InputHandlerComponent);

	EffectHandlerComponent = CreateDefaultSubobject<UBNBEffectHandlerComponent>(TEXT("Effect Handler Component"));
	AddOwnedComponent(EffectHandlerComponent);

	GameStateHandlerComponent = CreateDefaultSubobject<UBNBGameStateHandlerComponent>(
		TEXT("Game State Handler Component"));
	AddOwnedComponent(GameStateHandlerComponent);

	bIsPlaying = false;
	bIsAutoPlay = false;
	bIsDoublePoints = false;
	bIsContinueGame = false;
	bIsNewGame = false;
	
	LoadTimer = 4.f;
}

void ABNBPlayerController::BeginPlay()
{
	Super::BeginPlay();

	InitializeVideoSettings();
	InitializeGridSpawner();
	InitializeUI();

	SpawnMainBall();
	SpawnBonusBalls();
}

void ABNBPlayerController::InitializeVideoSettings()
{
	auto* GameSettings = UGameUserSettings::GetGameUserSettings();

	if (GameSettings)
	{
		if (!GameSettings->IsVSyncEnabled())
		{
			GameSettings->SetVSyncEnabled(true);
			GameSettings->ApplySettings(true);
		}

		IConsoleVariable* CVar;
		CVar = IConsoleManager::Get().FindConsoleVariable(TEXT("t.MaxFPS"));

		int32 Value;
		CVar->GetValue(Value);

		if (Value == 0)
		{
			CVar->Set(60);
		}
	}
}

void ABNBPlayerController::InitializeUI()
{
	if (!bIsContinueGame) SetUIInputMode();

	InitializePlayerUI();
	InitializeMainMenu();
	InitializeGameMenu();
	InitializeEndLevelScreen();
}

void ABNBPlayerController::InitializePlayerUI()
{
	if (IsValid(PlayerUIClass))
	{
		PlayerUIPtr = Cast<UWBNBPlayerUI>(CreateWidget(this, PlayerUIClass));

		if (PlayerUIPtr)
		{
			PlayerUIPtr->IncreaseLives(CurrentLives);
			PlayerUIPtr->SetScore(CurrentPoints);

			if (!bIsAutoPlay)
			{
				PlayerUIPtr->SetRecord(RecordPoints);
			}
			else
			{
				PlayerUIPtr->SetStartTextBlockVisibility(false);
				PlayerUIPtr->SetRecordScoreBlockVisibility(false);
			}

			if (InputHandlerComponent)
			{
				PlayerUIPtr->SetStartButton(InputHandlerComponent->GetStartKey());
			}

			PlayerUIPtr->AddToViewport();
		}
	}
}

void ABNBPlayerController::InitializeMainMenu()
{
	if (IsValid(MainMenuClass))
	{
		MainMenuPtr = Cast<UWBNBMainMenu>(CreateWidget(this, MainMenuClass));

		if (IsValid(MainMenuPtr))
		{
			MainMenuPtr->Initialization(this);
			MainMenuPtr->AddToViewport();

			if (bIsContinueGame || bIsNewGame)
			{
				MainMenuPtr->SetVisibility(ESlateVisibility::Hidden);
			}
		}
	}
}

void ABNBPlayerController::InitializeGameMenu()
{
	if (IsValid(GameMenuClass))
	{
		GameMenuPtr = Cast<UWBNBGameMenu>(CreateWidget(this, GameMenuClass));

		if (IsValid(GameMenuPtr))
		{
			GameMenuPtr->Initialization(this);
			GameMenuPtr->SetVisibility(ESlateVisibility::Hidden);
			GameMenuPtr->AddToViewport();
		}
	}
}

void ABNBPlayerController::InitializeEndLevelScreen()
{
	if (IsValid(EndLevelScreenClass))
	{
		EndLevelScreenPtr = Cast<UWBNBEndLevelScreen>(CreateWidget(this, EndLevelScreenClass));

		if (IsValid(EndLevelScreenPtr))
		{
			EndLevelScreenPtr->SetVisibility(ESlateVisibility::Hidden);
			EndLevelScreenPtr->AddToViewport();
		}
	}
}

void ABNBPlayerController::InitializeGridSpawner()
{
	if (IsValid(GameStateHandlerComponent) && IsValid(GameStateHandlerComponent->GetGameInstance()))
	{
		if (IsValid(
			BrickSpawner = Cast<ABNBBrickSpawner>(
				UGameplayStatics::GetActorOfClass(this, ABNBBrickSpawner::StaticClass()))))
		{
			if (bIsContinueGame)
			{
				BrickSpawner->Initialization(this, GameStateHandlerComponent->GetGameInstance(), CurrentPoints);
			}
			else
			{
				BrickSpawner->Initialization(this);
			}
		}
	}
}

void ABNBPlayerController::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	AutoPlay();
}

void ABNBPlayerController::AutoPlay()
{
	if (bIsAutoPlay && IsValid(MainBallPtr))
	{
		if (!bIsPlaying)
		{
			CallStartPlaying();
		}

		const FVector CurrentLocation = PlayerPawn->GetActorLocation();

		const FVector Direction(CurrentLocation.X, MainBallPtr->GetActorLocation().Y, CurrentLocation.Z);

		PlayerPawn->SetActorLocation(Direction, true);
		MainBallPtr->SetStartPosition(PlayerPawn->GetMainBallSpawnPos());

		if (Direction.Y > CurrentLocation.Y)
		{
			PlayerPawn->GetTrailEffects().Get<0>()->SetVisibility(true);
			PlayerPawn->GetTrailEffects().Get<1>()->SetVisibility(false);
		}
		else
		{
			PlayerPawn->GetTrailEffects().Get<1>()->SetVisibility(true);
			PlayerPawn->GetTrailEffects().Get<0>()->SetVisibility(false);
		}
	}
}

void ABNBPlayerController::SpawnMainBall()
{
	PlayerPawn = Cast<ABNBPawn>(GetPawn());

	if (IsValid(PlayerPawn))
	{
		PlayerPawn->Initialize(this);

		if (PlayerPawn->GetMainBallSpawnPos() != FVector::ZeroVector)
		{
			const FVector SpawnPos = FVector(PlayerPawn->GetMainBallSpawnPos().X, PlayerPawn->GetMainBallSpawnPos().Y,
			                                 PlayerPawn->GetActorLocation().Z);

			if (IsValid(GetWorld()))
			{
				const FRotator SpawnRot = FQuat::Identity.Rotator();

				FActorSpawnParameters SpawnParameters;
				SpawnParameters.Owner = PlayerPawn;
				SpawnParameters.SpawnCollisionHandlingOverride =
					ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButAlwaysSpawn;

				if (IsValid(MainBallClass))
				{
					MainBallPtr = GetWorld()->SpawnActor<ABNBMainBall>(MainBallClass, SpawnPos, SpawnRot,
					                                                   SpawnParameters);

					if (IsValid(MainBallPtr))
					{
						MainBallPtr->SetIsMainBall(true);
						MainBallPtr->SetStartPosition(SpawnPos);
						MainBallPtr->OnBallKilled.AddUObject(this, &ThisClass::HandleBallKilled);

						if (IsValid(InputHandlerComponent))
						{
							InputHandlerComponent->SetMainBall(MainBallPtr);
						}
						if (IsValid(EffectHandlerComponent))
						{
							EffectHandlerComponent->SetMainBall(MainBallPtr);
						}
					}
				}
			}
		}
	}
}

void ABNBPlayerController::SpawnBonusBalls()
{
	if (IsValid(GetWorld()))
	{
		if (IsValid(BonusBallClass))
		{
			for (int i = 0; i < 4; ++i)
			{
				bool bIsSpawned = false;

				do
				{
					const FRotator SpawnRot = FQuat::Identity.Rotator();
					const FVector SpawnPos(PlayerPawn->GetActorLocation().X + 100.f, FMath::RandRange(-900.f, 900.f),
					                       PlayerPawn->GetActorLocation().Z);
					FActorSpawnParameters SpawnParameters;
					SpawnParameters.Owner = PlayerPawn;
					SpawnParameters.SpawnCollisionHandlingOverride =
						ESpawnActorCollisionHandlingMethod::DontSpawnIfColliding;

					ABNBBonusBall* BonusBall = GetWorld()->SpawnActor<ABNBBonusBall>(
						BonusBallClass, SpawnPos, SpawnRot, SpawnParameters);

					if (BonusBall)
					{
						bIsSpawned = true;
						BonusBall->SetIsMainBall(false);
						BonusBall->SetActive(BonusBall, false);
						BonusBall->SetIsPlaying(false);
						BonusBall->SetStartPosition(SpawnPos);

						OnEnableBonusBalls.AddUObject(BonusBall, &ABNBBonusBall::EnableBall);
					}
				}
				while (!bIsSpawned);
			}
		}
	}
}

void ABNBPlayerController::SetUIInputMode()
{
	FInputModeGameAndUI InputMode;
	InputMode.SetHideCursorDuringCapture(false);
	InputMode.SetLockMouseToViewportBehavior(EMouseLockMode::DoNotLock);
	SetInputMode(InputMode);
	SetShowMouseCursor(true);
}

void ABNBPlayerController::SetGameInputMode()
{
	FInputModeGameOnly InputMode;
	InputMode.SetConsumeCaptureMouseDown(false);
	SetInputMode(InputMode);
	SetShowMouseCursor(false);
}

void ABNBPlayerController::CallHideStartText(const bool& Value)
{
	if (IsValid(PlayerUIPtr))
	{
		PlayerUIPtr->SetStartTextBlockVisibility(!Value);
	}
}

void ABNBPlayerController::CallSetRecordScoreBlockVisibility(const bool& Value)
{
	if (IsValid(PlayerUIPtr))
	{
		PlayerUIPtr->SetRecordScoreBlockVisibility(!Value);
	}
}

void ABNBPlayerController::ShowGameMenu()
{
	if (IsValid(GameMenuPtr))
	{
		if (GameMenuPtr->GetVisibility() == ESlateVisibility::Visible)
		{
			GameMenuPtr->SetVisibility(ESlateVisibility::Hidden);

			UGameplayStatics::SetGamePaused(this, false);
			SetGameInputMode();
		}
		else
		{
			GameMenuPtr->SetVisibility(ESlateVisibility::Visible);

			UGameplayStatics::SetGamePaused(this, true);
			SetUIInputMode();
		}
	}
}

void ABNBPlayerController::IncreaseLives(const int32& _CurrentLives)
{
	if (CurrentLives <= MaxLives)
	{
		CurrentLives += _CurrentLives;

		if (IsValid(PlayerUIPtr))
		{
			PlayerUIPtr->IncreaseLives(CurrentLives);
		}
	}
}

void ABNBPlayerController::CallSetRecord(const int32& Value)
{
	if (IsValid(PlayerUIPtr))
	{
		PlayerUIPtr->SetRecord(Value);
	}
}

void ABNBPlayerController::CallShowEffectText(const EBNBEffectType& EffectType, const bool& Value)
{
	if (IsValid(PlayerUIPtr))
	{
		PlayerUIPtr->SetEffectVisibility(EffectType, Value);
	}
}

void ABNBPlayerController::IncreasePoints(int32 Points)
{
	if (bIsDoublePoints)
	{
		Points *= 2;
	}

	CurrentPoints += Points;

	if (IsValid(PlayerUIPtr))
	{
		PlayerUIPtr->SetScore(CurrentPoints);
	}

	if (CurrentPoints >= RecordPoints && !bIsAutoPlay)
	{
		if (IsValid(PlayerUIPtr) && IsValid(GameStateHandlerComponent))
		{
			PlayerUIPtr->SetRecord(CurrentPoints);
			GameStateHandlerComponent -> SaveRecord(CurrentPoints);
		}
	}
}

void ABNBPlayerController::HandleBallKilled(const bool& _bIsPlaying)
{
	bIsPlaying = _bIsPlaying;
	CurrentLives--;

	if (IsValid(BrickSpawner))
	{
		BrickSpawner->CallDeactivateEffects();
	}

	ResetEffects();

	if (IsValid(PlayerUIPtr))
	{
		PlayerUIPtr->DecreaseLives();

		if (CurrentLives > 0 && !bIsAutoPlay)
		{
			PlayerUIPtr->SetStartTextBlockVisibility(true);
		}
	}

	if (CurrentLives == 0)
	{
		if (IsValid(GameStateHandlerComponent))
		{
			GameStateHandlerComponent->GameOver(CurrentPoints, CurrentPoints > RecordPoints);
			HandleEndLevelSequence(false);
		}
	}
}


void ABNBPlayerController::HandleLevelCompleted(const bool& _bIsPlaying)
{
	bIsPlaying = _bIsPlaying;

	if (IsValid(MainBallPtr))
	{
		MainBallPtr ->SetIsPlaying(bIsPlaying);
	}
	
	ResetEffects();

	if (IsValid(GameStateHandlerComponent) && IsValid(BrickSpawner))
	{
		if (!bIsAutoPlay)
		{
			GameStateHandlerComponent->LevelCompleted(CurrentPoints, BrickSpawner->GetXRows(),
			                                          BrickSpawner->GetYColumns(), CurrentPoints > RecordPoints);
		}
		else
		{
			GameStateHandlerComponent->LevelCompleted(CurrentPoints, BrickSpawner->GetXRows(),
			                                          BrickSpawner->GetYColumns(), false);
		}

		HandleEndLevelSequence(true);
	}
}

void ABNBPlayerController::HandleEndLevelSequence(const bool& bIsWin)
{
	if (bIsAutoPlay) bIsAutoPlay = false;

	if (IsValid(GetWorld()))
	{
		FTimerHandle TimerHandle;
		GetWorld()->GetTimerManager().SetTimer(TimerHandle, [&] { RestartLevel(); }, LoadTimer, false);
	}

	if (IsValid(EndLevelScreenPtr) && IsValid(InputHandlerComponent))
	{
		InputHandlerComponent -> SetIsCompleted(true);
		EndLevelScreenPtr->ShowScreen(bIsWin);
		EndLevelScreenPtr->SetVisibility(ESlateVisibility::Visible);
	}
}

void ABNBPlayerController::CallStartGame(const bool& _bIsManualGame)
{
	if (IsValid(GameStateHandlerComponent))
	{
		GameStateHandlerComponent->StartGame(_bIsManualGame);
	}
}

void ABNBPlayerController::CallStartNewGame(const bool& _bIsManualGame)
{
	if (IsValid(GameStateHandlerComponent) && IsValid(BrickSpawner))
	{
		GameStateHandlerComponent->StartNewGame(BrickSpawner->GetMinXRows(), BrickSpawner->GetMinYColumns(),
		                                        _bIsManualGame);
		RestartLevel();
	}
}

void ABNBPlayerController::CallResumeGame()
{
	if (IsValid(GameStateHandlerComponent))
	{
		GameStateHandlerComponent->ResumeGame();
	}
}

void ABNBPlayerController::CallExitGame()
{
	if (IsValid(GameStateHandlerComponent))
	{
		GameStateHandlerComponent->ExitGame();
	}
}

void ABNBPlayerController::ResetEffects()
{
	if (MainBallPtr)
	{
		if (MainBallPtr->GetIsFireball())
		{
			MainBallPtr->DisableFireball();
		}
		if (MainBallPtr->GetIsSpeedIncreased())
		{
			MainBallPtr->SetCurrentBallSpeed(MainBallPtr->GetBaseBallSpeed());
		}
	}

	if (bIsDoublePoints)
	{
		bIsDoublePoints = false;
	}

	OnEnableBonusBalls.Broadcast(false);
}

void ABNBPlayerController::CallStartPlaying()
{
	bIsPlaying = true;

	if (MainBallPtr)
	{
		MainBallPtr->SetIsPlaying(GetIsPlaying());
	}
}
