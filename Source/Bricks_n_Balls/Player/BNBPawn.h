// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ArrowComponent.h"
#include "GameFramework/Pawn.h"
#include "Bricks_n_Balls/Structs/BNBEffectData.h"
#include "BNBPawn.generated.h"

class UCapsuleComponent;
class UStaticMeshComponent;
class UArrowComponent;
class UNiagaraSystem;
class UNiagaraComponent;
class ABNBPlayerController;

UCLASS()
class BRICKS_N_BALLS_API ABNBPawn : public APawn
{
	GENERATED_BODY()

public:
	ABNBPawn();

private:
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	void Initialize(ABNBPlayerController* _PlayerController);
	void CallApplyEffect(const FBNBEffectData& EffectData) const;

	FORCEINLINE FVector GetMainBallSpawnPos() const
	{
		if (ArrowComponent)
		{
			return ArrowComponent -> GetComponentLocation();
		}
		
		return FVector::ZeroVector;
	}

	FORCEINLINE float GetCurrentPawnSpeed() const { return CurrentPawnSpeed; }
	FORCEINLINE void SetCurrentPawnSpeed(const float& Value) { CurrentPawnSpeed = Value; }
	FORCEINLINE void SetCurrentSpeedToBase() { CurrentPawnSpeed = BasePawnSpeed; }
	
	FORCEINLINE TTuple<UNiagaraComponent*, UNiagaraComponent*> GetTrailEffects() { return MakeTuple(LeftTrailEffect, RightTrailEffect); }
	
private:
	UPROPERTY()
	ABNBPlayerController* PlayerController;
	UPROPERTY(EditDefaultsOnly)
	UCapsuleComponent* CapsuleComponent;
	UPROPERTY(EditDefaultsOnly)
	UStaticMeshComponent* MeshComponent;
	UPROPERTY(EditDefaultsOnly)
	UArrowComponent* ArrowComponent;
	UPROPERTY(EditDefaultsOnly)
	UArrowComponent* LeftTrailPoint;
	UPROPERTY(EditDefaultsOnly)
	UArrowComponent* RightTrailPoint;
	UPROPERTY(EditDefaultsOnly, Category="Settings | Movement")
	float CurrentPawnSpeed;
	UPROPERTY(EditDefaultsOnly, Category="Settings | Movement")
	float BasePawnSpeed;
	UPROPERTY(EditDefaultsOnly, Category="Settings | Effect")
	UNiagaraSystem* TrailEffect;
	UPROPERTY(VisibleAnywhere)
	UNiagaraComponent* LeftTrailEffect;
	UPROPERTY(VisibleAnywhere)
	UNiagaraComponent* RightTrailEffect;
protected:
};
