// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "Bricks_n_Balls/Helper/DelegatesHolder.h"
#include "BNBPlayerController.generated.h"

class UWBNBEndLevelScreen;
class UWBNBGameMenu;
class UWBNBMainMenu;
class UWBNBPlayerUI;
class UBNBInputHandlerComponent;
class UBNBEffectHandlerComponent;
class UBNBGameStateHandlerComponent;
class ABNBBrickSpawner;
class ABNBBonusBall;
class ABNBPawn;
class ABNBMainBall;
class UBNBGameInstance;

enum class EBNBEffectType : uint8;

UCLASS()
class BRICKS_N_BALLS_API ABNBPlayerController : public APlayerController
{
	GENERATED_BODY()

	ABNBPlayerController();

private:
	void InitializeVideoSettings();
	void InitializeGridSpawner();
	void InitializeUI();
	void InitializePlayerUI();
	void InitializeMainMenu();
	void InitializeGameMenu();
	void InitializeEndLevelScreen();
	void SpawnMainBall();
	void SpawnBonusBalls();
	void ResetEffects();
	void CallStartPlaying();

protected:
	virtual void BeginPlay() override;
	void AutoPlay();
	virtual void Tick(float DeltaSeconds) override;
	
public:
	FOnApplyEffect OnApplyEffect;
	FOnEnableBonusBalls OnEnableBonusBalls;
	
	void SetUIInputMode();
	void SetGameInputMode();

	void HandleBallKilled(const bool& _bIsPlaying);
	void HandleLevelCompleted(const bool& _bIsPlaying);

	void CallStartGame(const bool& _bIsManualGame);
	void CallStartNewGame(const bool& _bIsManualGame);
	void CallResumeGame();
	void CallExitGame();
	
	void IncreasePoints(int32 Points);
	/**
	 * Sets the game's paused state
	 * @param	bIsWin	Whether the level was completed or was game over
	 */
	void HandleEndLevelSequence(const bool& bIsWin);
	void IncreaseLives(const int32& _CurrentLives);

	void CallSetRecord(const int32& Value);
	void CallShowEffectText(const EBNBEffectType& EffectType, const bool& Value);
	void CallHideStartText(const bool& Value);
	void CallSetRecordScoreBlockVisibility(const bool& Value);
	void ShowGameMenu();
	
	FORCEINLINE bool GetIsAutoPlay() const { return bIsAutoPlay; }
	FORCEINLINE bool GetIsPlaying() const { return bIsPlaying; }
	FORCEINLINE int32 GetCurrentLives() const { return CurrentLives; }
	FORCEINLINE int32 GetMaxLives() const { return MaxLives; }
	FORCEINLINE int32 GetCurrentPoints() const { return CurrentPoints; }
	FORCEINLINE int32 GetRecordPoints() const { return  RecordPoints; }
	
	FORCEINLINE void SetIsContinueGame(const bool& _bIsContinueGame) { bIsContinueGame = _bIsContinueGame; }
	FORCEINLINE void SetIsNewGame(const bool& _bIsNewGame) { bIsNewGame = _bIsNewGame; }
	FORCEINLINE void SetIsPlaying(const bool& _bIsPlaying) { bIsPlaying = _bIsPlaying; }
	FORCEINLINE void SetIsAutoPlay(const bool& _bIsAutoPlay) { bIsAutoPlay = _bIsAutoPlay; }
	FORCEINLINE void SetIsDoublePoints(const bool& _bIsDoublePoints) { bIsDoublePoints = _bIsDoublePoints; }
	FORCEINLINE void SetCurrentPoints(const int32& _CurrentPoints) { CurrentPoints = _CurrentPoints; }
	FORCEINLINE void SetRecordPoints(const int32& _RecordPoints) { RecordPoints = _RecordPoints; }

private:
	UPROPERTY()
	ABNBPawn* PlayerPawn;
	UPROPERTY(VisibleAnywhere)
	UBNBInputHandlerComponent* InputHandlerComponent;
	UPROPERTY(VisibleAnywhere)
	UBNBEffectHandlerComponent* EffectHandlerComponent;
	UPROPERTY(VisibleAnywhere)
	UBNBGameStateHandlerComponent* GameStateHandlerComponent;
	UPROPERTY()
	ABNBBrickSpawner* BrickSpawner;
	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<ABNBMainBall> MainBallClass;
	UPROPERTY()
	ABNBMainBall* MainBallPtr;
	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<ABNBBonusBall> BonusBallClass;
	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<UWBNBPlayerUI> PlayerUIClass;
	UPROPERTY()
	UWBNBPlayerUI* PlayerUIPtr;
	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<UWBNBMainMenu> MainMenuClass;
	UPROPERTY()
	UWBNBMainMenu* MainMenuPtr;
	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<UWBNBGameMenu> GameMenuClass;
	UPROPERTY()
	UWBNBGameMenu* GameMenuPtr;
	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<UWBNBEndLevelScreen> EndLevelScreenClass;
	UPROPERTY()
	UWBNBEndLevelScreen* EndLevelScreenPtr;

	UPROPERTY(VisibleAnywhere)
	bool bIsContinueGame;
	UPROPERTY(VisibleAnywhere)
	bool bIsNewGame;
	UPROPERTY(VisibleAnywhere)
	bool bIsPlaying;
	UPROPERTY(EditAnywhere)
	bool bIsAutoPlay;
	UPROPERTY(VisibleAnywhere)
	bool bIsDoublePoints;
	UPROPERTY(VisibleAnywhere)
	int32 CurrentPoints;
	UPROPERTY(VisibleAnywhere)
	int32 RecordPoints;
	UPROPERTY(VisibleAnywhere)
	int32 CurrentLives;
	UPROPERTY(VisibleAnywhere)
	int32 MaxLives;
	UPROPERTY()
	float LoadTimer;
protected:
	
};