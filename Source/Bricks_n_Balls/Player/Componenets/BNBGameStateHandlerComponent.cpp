// Fill out your copyright notice in the Description page of Project Settings.


#include "BNBGameStateHandlerComponent.h"
#include "Bricks_n_Balls/Player/BNBPlayerController.h"
#include "Bricks_n_Balls/Core/BNBGameInstance.h"
#include "Bricks_n_Balls/Core/BNBSaveGame.h"
#include "Kismet/GameplayStatics.h"

// Sets default values for this component's properties
UBNBGameStateHandlerComponent::UBNBGameStateHandlerComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = false;

	// ...
}

// Called when the game starts
void UBNBGameStateHandlerComponent::BeginPlay()
{
	Super::BeginPlay();

	GameStarted();
}

void UBNBGameStateHandlerComponent::GameStarted()
{
	if (IsValid(GetWorld()))
	{
		GameInstance = Cast<UBNBGameInstance>(UGameplayStatics::GetGameInstance(GetWorld()));
		PlayerController = Cast<ABNBPlayerController>(GetOwner());

		if (IsValid(GameInstance) && IsValid(PlayerController))
		{
			const auto* GameData = GameInstance->LoadGameData();

			if (IsValid(GameData))
			{
				PlayerController->SetIsContinueGame(GameInstance->GetIsContinueGame());
				PlayerController->SetIsNewGame(GameInstance->GetIsNewGame());
				PlayerController->SetCurrentPoints(GameInstance->GetCurrentPoints());
				PlayerController->SetIsAutoPlay(GameInstance->GetIsAutoPlay());
				PlayerController->SetRecordPoints(GameData->GetRecordPoints());
			}

			if (GameInstance->GetIsContinueGame() || GameInstance->GetIsNewGame())
			{
				if (IsValid(PlayerController))
				{
					PlayerController->SetGameInputMode();
				}

				UGameplayStatics::SetGamePaused(this, false);
			}
			else
			{
				UGameplayStatics::SetGamePaused(this, true);
			}
		}
	}
}

void UBNBGameStateHandlerComponent::SaveRecord(const int32& CurrentPoints)
{
	if (IsValid(GameInstance))
	{
		GameInstance -> SaveGameData(CurrentPoints);
	}
}

void UBNBGameStateHandlerComponent::GameOver(const int32& CurrentPoints, const bool& bIsRecord)
{
	if (IsValid(GameInstance))
	{
		GameInstance->SetIsContinueGame(false);
		GameInstance->SetIsNewGame(false);
		GameInstance->SetCurrentPoints(0);

		if (bIsRecord && !PlayerController->GetIsAutoPlay())
		{
			GameInstance->SaveGameData(CurrentPoints);
		}
	}
}

void UBNBGameStateHandlerComponent::LevelCompleted(const int32& CurrentPoints, const int32& CurrentXRows,
                                                   const int32& CurrentYColumns, const bool& bIsRecord)
{
	if (IsValid(GameInstance) && IsValid(PlayerController))
	{
		GameInstance->SetIsContinueGame(true);
		GameInstance->SetIsNewGame(false);
		GameInstance->SetIsAutoPlay(PlayerController->GetIsAutoPlay());
		GameInstance->SetCurrentPoints(CurrentPoints);
		GameInstance->SetCurrentXRows(CurrentXRows);
		GameInstance->SetCurrentYColumns(CurrentYColumns);

		if (bIsRecord && !PlayerController->GetIsAutoPlay())
		{
			GameInstance->SaveGameData(CurrentPoints);
		}
	}
}

void UBNBGameStateHandlerComponent::StartNewGame(const int32& XRows, const int32& YColumns, const bool& bIsManualGame)
{
	if (IsValid(GameInstance) && IsValid(PlayerController))
	{
		
		GameInstance->SetIsNewGame(true);
		GameInstance->SetIsContinueGame(false);
		GameInstance->SetCurrentPoints(0);
		GameInstance->SetCurrentXRows(XRows);
		GameInstance->SetCurrentYColumns(YColumns);
		GameInstance->SetIsAutoPlay(!bIsManualGame);

		if (PlayerController->GetCurrentPoints() > PlayerController->GetRecordPoints() && !PlayerController -> GetIsAutoPlay())
		{
			GameInstance->SaveGameData(PlayerController->GetCurrentPoints());
		}
	}
}

void UBNBGameStateHandlerComponent::StartGame(const bool& bIsManualGame)
{
	if (IsValid(PlayerController))
	{
		PlayerController->SetIsAutoPlay(!bIsManualGame);
		PlayerController->SetGameInputMode();
		PlayerController->CallHideStartText(!bIsManualGame);
		PlayerController->CallSetRecordScoreBlockVisibility(!bIsManualGame);

		if (UGameplayStatics::IsGamePaused(this))
		{
			UGameplayStatics::SetGamePaused(this, false);
		}
	}
}

void UBNBGameStateHandlerComponent::ResumeGame()
{
	if (IsValid(PlayerController))
	{
		PlayerController->SetGameInputMode();

		if (UGameplayStatics::IsGamePaused(this))
		{
			UGameplayStatics::SetGamePaused(this, false);
		}
	}
}

void UBNBGameStateHandlerComponent::ExitGame()
{
	if (IsValid(GetWorld()) && IsValid(PlayerController))
	{
		UKismetSystemLibrary::QuitGame(GetWorld(), PlayerController, EQuitPreference::Quit, false);
	}
}
