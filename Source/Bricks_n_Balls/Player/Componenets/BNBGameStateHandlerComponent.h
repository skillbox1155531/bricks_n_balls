// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "BNBGameStateHandlerComponent.generated.h"

class UBNBGameInstance;
class ABNBPlayerController;


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class BRICKS_N_BALLS_API UBNBGameStateHandlerComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UBNBGameStateHandlerComponent();
	void GameStarted();

	void SaveRecord(const int32& CurrentPoints);
	void GameOver(const int32& CurrentPoints, const bool& bIsRecord);
	void LevelCompleted(const int32& CurrentPoints, const int32& CurrentXRows, const int32& CurrentYColumns, const bool& bIsRecord);
	/**
	 * Sets the game's paused state
	 * @param	bIsManualGame	Whether the Manual game should be started or Auto
	 */
	void StartNewGame(const int32& XRows, const int32& YColumns, const bool& bIsManualGame);
	/**
	 * Sets the game's paused state
	 * @param	bIsManualGame	Whether the Manual game should be started or Auto
	 */
	void StartGame(const bool& bIsManualGame);
	void ResumeGame();
	void ExitGame();

	FORCEINLINE UBNBGameInstance* GetGameInstance() const { return GameInstance; }
protected:
	// Called when the game starts
	virtual void BeginPlay() override;
private:
	UPROPERTY()
	UBNBGameInstance* GameInstance;
	UPROPERTY()
	ABNBPlayerController* PlayerController;
};
