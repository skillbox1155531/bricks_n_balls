// Fill out your copyright notice in the Description page of Project Settings.

#include "BNBInputHandlerComponent.h"
#include "EnhancedInputComponent.h"
#include "EnhancedInputSubsystems.h"
#include "InputMappingContext.h"
#include "NiagaraComponent.h"
#include "Bricks_n_Balls/Ball/BNBMainBall.h"
#include "Bricks_n_Balls/Player/BNBPawn.h"
#include "Bricks_n_Balls/Player/BNBPlayerController.h"
#include "Components/SphereComponent.h"

// Sets default values for this component's properties
UBNBInputHandlerComponent::UBNBInputHandlerComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = false;

	BNBPlayerController = nullptr;
	PlayerPawn = nullptr;
	MainBall = nullptr;
	bIsCompleted = false;
	
	BaseContext = LoadObject<UInputMappingContext>(NULL, *BaseContextPath, NULL, LOAD_None, NULL);
	IA_Horizontal = LoadObject<UInputAction>(NULL, *IA_HorizontalPath, NULL, LOAD_None, NULL);
	IA_Start = LoadObject<UInputAction>(NULL, *IA_StartPath, NULL, LOAD_None, NULL);
	IA_GameMenu = LoadObject<UInputAction>(NULL, *IA_GameMenuPath, NULL, LOAD_None, NULL);
	ContextPriority = 0;
}

// Called when the game starts
void UBNBInputHandlerComponent::BeginPlay()
{
	Super::BeginPlay();
	
	if (IsValid(Cast<ABNBPlayerController>(GetOwner())))
	{
		BNBPlayerController = Cast<ABNBPlayerController>(GetOwner());
		PlayerPawn = Cast<ABNBPawn>(BNBPlayerController -> GetPawn());
		
		InitializeInput(BNBPlayerController);
	}

}

void UBNBInputHandlerComponent::InitializeInput(ABNBPlayerController* PlayerController)
{
	AddInputContext(PlayerController);
	BindInputEvents(PlayerController);
}

void UBNBInputHandlerComponent::AddInputContext(ABNBPlayerController* PlayerController) const
{
	if (IsValid(PlayerController))
	{
		UEnhancedInputLocalPlayerSubsystem* Subsystem = ULocalPlayer::GetSubsystem<UEnhancedInputLocalPlayerSubsystem>(PlayerController -> GetLocalPlayer());

		if (IsValid(Subsystem))
		{
			if (BaseContext.IsValid())
			{
				Subsystem -> AddMappingContext(BaseContext.LoadSynchronous(), ContextPriority);
			}
			else
			{
				UE_LOG(LogTemp, Error, TEXT("No Input Mapping Context found"));
			}
		}
		else
		{
			UE_LOG(LogTemp, Error, TEXT("No Subsystem found"));
		}	
	}
}

void UBNBInputHandlerComponent::BindInputEvents(ABNBPlayerController* PlayerController)
{
	auto* InputComponent = Cast<UEnhancedInputComponent>(PlayerController -> InputComponent);

	if (IsValid(InputComponent))
	{
		if (IsValid(IA_Horizontal) && IsValid(IA_Start) && IsValid(IA_GameMenu))
		{
			InputComponent -> BindAction(IA_Horizontal, ETriggerEvent::Triggered, this, &ThisClass::HandleHorizontalInput);
			InputComponent -> BindAction(IA_Horizontal, ETriggerEvent::Completed, this, &ThisClass::HandleStopHorizontalInput);
			InputComponent -> BindAction(IA_Start, ETriggerEvent::Triggered, this, &ThisClass::HandleStartPlay);
			InputComponent -> BindAction(IA_GameMenu, ETriggerEvent::Completed, this, &ThisClass::HandleGameMenuInput);
		}
	}
}

void UBNBInputHandlerComponent::HandleHorizontalInput(const FInputActionValue& Value)
{
	const float Input = Value.Get<float>();

	if (IsValid(PlayerPawn) && IsValid(BNBPlayerController) && IsValid(GetWorld()))
	{
		if (!BNBPlayerController -> GetIsAutoPlay() && !bIsCompleted)
		{
			const float DeltaSpeed =  PlayerPawn -> GetCurrentPawnSpeed() * GetWorld() -> GetDeltaSeconds();
			const FVector Direction(0.f, Input * DeltaSpeed, 0.f);
			PlayerPawn -> AddActorWorldOffset(Direction, true);
			
			if (IsValid(MainBall))
			{
				MainBall -> SetStartPosition(PlayerPawn -> GetMainBallSpawnPos());
				
				if (!BNBPlayerController -> GetIsPlaying())
				{
					MainBall -> SetActorLocation(PlayerPawn -> GetMainBallSpawnPos());
				}
			}

			PlayTrailEffect(Input);
		}
	}
}

void UBNBInputHandlerComponent::PlayTrailEffect(const float& Input)
{
	if (Input > 0)
	{
		if (IsValid(PlayerPawn -> GetTrailEffects().Get<0>()))
		{
			PlayerPawn -> GetTrailEffects().Get<0>() -> SetVisibility(true);
		}
	}
	else
	{
		if (IsValid(PlayerPawn -> GetTrailEffects().Get<1>()))
		{
			PlayerPawn -> GetTrailEffects().Get<1>() -> SetVisibility(true);
		}
	}
}

void UBNBInputHandlerComponent::HandleStopHorizontalInput()
{
	if (IsValid(PlayerPawn -> GetTrailEffects().Get<1>()))
	{
		if (PlayerPawn -> GetTrailEffects().Get<1>() -> IsVisible())
		{
			PlayerPawn -> GetTrailEffects().Get<1>() -> SetVisibility(false);
		}
	}
	if (IsValid(PlayerPawn -> GetTrailEffects().Get<0>()))
	{
		if (PlayerPawn -> GetTrailEffects().Get<0>() -> IsVisible())
		{
			PlayerPawn -> GetTrailEffects().Get<0>() -> SetVisibility(false);
		}
	}
}

void UBNBInputHandlerComponent::HandleGameMenuInput()
{
	if (IsValid(BNBPlayerController))
	{
		if (!bIsCompleted)
		{
			BNBPlayerController -> ShowGameMenu();
		}
	}
}

void UBNBInputHandlerComponent::HandleStartPlay()
{
	if (IsValid(BNBPlayerController))
	{
		BNBPlayerController -> SetIsPlaying(true);
		BNBPlayerController -> CallHideStartText(true);
		
		if (IsValid(MainBall))
		{
			MainBall -> SetIsPlaying(true);
		}
	}
}
