// Fill out your copyright notice in the Description page of Project Settings.

#include "BNBEffectHandlerComponent.h"
#include "Bricks_n_Balls/Ball/BNBMainBall.h"
#include "Kismet/GameplayStatics.h"
#include "Bricks_n_Balls/Player/BNBPawn.h"
#include "Bricks_n_Balls/Player/BNBPlayerController.h"
#include "Bricks_n_Balls/Spawners/BNBBrickSpawner.h"
#include "Bricks_n_Balls/Structs/BNBEffectData.h"

// Sets default values for this component's properties
UBNBEffectHandlerComponent::UBNBEffectHandlerComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = false;
	
	BNBPlayerController = nullptr;
	PlayerPawn = nullptr;
	MainBall = nullptr;
	BrickSpawner = nullptr;
	
	FireballTimer = 0.f;
	FireballDuration = 0.f;

	DoublePointsTimer = 0.f;
	DoublePointsDuration = 0.f;

	IncreasedBallSpeedTimer = 0.f;
	IncreasedBallSpeedDuration = 0.f;
}

// Called when the game starts
void UBNBEffectHandlerComponent::BeginPlay()
{
	Super::BeginPlay();

	if (IsValid(Cast<ABNBPlayerController>(GetOwner())))
	{
		BNBPlayerController = Cast<ABNBPlayerController>(GetOwner());
		PlayerPawn = Cast<ABNBPawn>(BNBPlayerController -> GetPawn());
		BrickSpawner = Cast<ABNBBrickSpawner>(UGameplayStatics::GetActorOfClass(this, ABNBBrickSpawner::StaticClass()));
		
		BNBPlayerController -> OnApplyEffect.BindUObject(this, &ThisClass::HandleEffect);
	}
}

void UBNBEffectHandlerComponent::HandleEffect(const FBNBEffectData& EffectData)
{
	if (EffectData.GetEffectType() == EBNBEffectType::ET_ExtendPaddleSize)
	{
		ExtendPaddleSize(EffectData.GetEffectValue());
	}
	else if(EffectData.GetEffectType() == EBNBEffectType::ET_AdditionalBalls)
	{
		CallEnableBonusBalls();
	}
	else if (EffectData.GetEffectType() == EBNBEffectType::ET_IncreaseMainBallSize)
	{
		ExtendMainBallSize(EffectData.GetEffectValue());
	}
	else if (EffectData.GetEffectType() == EBNBEffectType::ET_Fireball)
	{
		EnableFireball(EffectData.GetEffectType(), EffectData.GetEffectDuration());
	}
	else if (EffectData.GetEffectType() == EBNBEffectType::ET_DoublePoints)
	{
		EnableDoublePoints(EffectData.GetEffectType(), EffectData.GetEffectDuration());
	}
	else if (EffectData.GetEffectType() == EBNBEffectType::ET_AddLife)
	{
		AddLives(EffectData.GetEffectValue());
	}
	else if (EffectData.GetEffectType() == EBNBEffectType::ET_ShrinkPaddleSize)
	{
		ShrinkPaddleSize(EffectData.GetEffectValue());
	}
	else if (EffectData.GetEffectType() == EBNBEffectType::ET_ShrinkBallSize)
	{
		ShrinkMainBallSize(EffectData.GetEffectValue());
	}
	else if (EffectData.GetEffectType() == EBNBEffectType::ET_DecreasePaddleSpeed)
	{
		DecreasePaddleSpeed(EffectData.GetEffectType(), EffectData.GetEffectValue(), EffectData.GetEffectDuration());
	}
	else if (EffectData.GetEffectType() == EBNBEffectType::ET_IncreaseBallSpeed)
	{
		IncreaseMainBallSpeed(EffectData.GetEffectType(), EffectData.GetEffectValue(), EffectData.GetEffectDuration());
	}
}

void UBNBEffectHandlerComponent::ExtendPaddleSize(const float& Value)
{
	if (PlayerPawn)
	{
		const FVector PaddleScale = PlayerPawn -> GetActorScale3D();	
		const FVector NewScale(PaddleScale.X, PaddleScale.Y, PaddleScale.Z  + Value);

		PlayerPawn -> SetActorScale3D(NewScale);
	}
}

void UBNBEffectHandlerComponent::ShrinkPaddleSize(const float& Value)
{
	if (PlayerPawn)
	{
		const FVector PaddleScale = PlayerPawn -> GetActorScale3D();	
		const FVector NewScale(PaddleScale.X, PaddleScale.Y, PaddleScale.Z  - Value);
		
		PlayerPawn -> SetActorScale3D(NewScale);
	}
}

void UBNBEffectHandlerComponent::ExtendMainBallSize(const float& Value)
{
	if (IsValid(MainBall))
	{
		const FVector PaddleScale = MainBall -> GetActorScale3D();	
		const FVector NewScale(PaddleScale.X + Value, PaddleScale.Y + Value, PaddleScale.Z + Value);
		
		MainBall -> SetActorScale3D(NewScale);
	}
}

void UBNBEffectHandlerComponent::ShrinkMainBallSize(const float& Value)
{
	if (IsValid(MainBall))
	{
		const FVector PaddleScale = MainBall -> GetActorScale3D();	
		const FVector NewScale(PaddleScale.X - Value, PaddleScale.Y - Value, PaddleScale.Z - Value);
		
		MainBall -> SetActorScale3D(NewScale);
	}
}

void UBNBEffectHandlerComponent::EnableFireball(const EBNBEffectType& EffectData, const float& DurationValue)
{
	if (IsValid(MainBall) && IsValid(BrickSpawner) && IsValid(BNBPlayerController))
	{
		MainBall -> EnableFireball();
		BrickSpawner -> CallEnableCollision(false);
		FireballDuration = DurationValue;
		
		BNBPlayerController -> CallShowEffectText(EffectData, true);
		
		CreateFireballBuffTimer();
	}
}

void UBNBEffectHandlerComponent::EnableDoublePoints(const EBNBEffectType& EffectData, const float& DurationValue)
{
	if (IsValid(BNBPlayerController))
	{
		BNBPlayerController -> SetIsDoublePoints(true);
		DoublePointsDuration = DurationValue;

		BNBPlayerController -> CallShowEffectText(EffectData, true);
		
		CreateDoublePointsBuffTimer();
	}
}

void UBNBEffectHandlerComponent::AddLives(const float& Value)
{
	if (IsValid(BNBPlayerController))
	{
		if (BNBPlayerController -> GetCurrentLives() != BNBPlayerController -> GetMaxLives())
		{
			BNBPlayerController -> IncreaseLives(Value);
		}
	}
}

void UBNBEffectHandlerComponent::DecreasePaddleSpeed(const EBNBEffectType& EffectData, const float& Value, const float& DurationValue)
{
	if (IsValid(PlayerPawn) && IsValid(BNBPlayerController))
	{
		DecreasePaddleSpeedDuration = DurationValue;
		PlayerPawn -> SetCurrentPawnSpeed(Value);
		BNBPlayerController -> CallShowEffectText(EffectData, true);
		CreateDecreasePaddleSpeedDebuffTimer();
	}
}

void UBNBEffectHandlerComponent::IncreaseMainBallSpeed(const EBNBEffectType& EffectData, const float& Value, const float& DurationValue)
{
	if (IsValid(PlayerPawn) && IsValid(BNBPlayerController))
	{
		IncreasedBallSpeedDuration = DurationValue;
		MainBall -> SetIsSpeedIncreased(true);
		MainBall -> SetCurrentBallSpeed(Value);
		BNBPlayerController -> CallShowEffectText(EffectData, true);
		
		CreateIncreaseBallSpeedDebuffTimer();
	}
}

void UBNBEffectHandlerComponent::CallEnableBonusBalls()
{
	if (IsValid(BNBPlayerController))
	{
		BNBPlayerController -> OnEnableBonusBalls.Broadcast(true);
	}
}

void UBNBEffectHandlerComponent::CreateFireballBuffTimer()
{
	FireballTimer = FireballDuration;
	
	if(IsValid(GetWorld()))
	{
		if (GetWorld() -> GetTimerManager().TimerExists(FireballTimerHandle))
		{
			GetWorld() -> GetTimerManager().ClearTimer(FireballTimerHandle);
		}
		
		GetWorld() -> GetTimerManager().SetTimer(FireballTimerHandle, this, &ThisClass::FireballBuffTimer, 1.f, true, 0.f);	
	}
}

void UBNBEffectHandlerComponent::FireballBuffTimer()
{
	FireballTimer--;

	if (FireballTimer == 0.f)
	{
		if(IsValid(MainBall) && IsValid(BrickSpawner) && IsValid(BNBPlayerController))
		{
			MainBall -> DisableFireball();
			BrickSpawner -> CallEnableCollision(true);
			BNBPlayerController -> CallShowEffectText(EBNBEffectType::ET_Fireball, false);
			
			if(IsValid(GetWorld()))
			{
				GetWorld() -> GetTimerManager().ClearTimer(FireballTimerHandle);
			}
		}
	}
}

void UBNBEffectHandlerComponent::CreateDoublePointsBuffTimer()
{
	DoublePointsTimer = DoublePointsDuration;
	
	if(IsValid(GetWorld()))
	{
		if (GetWorld() -> GetTimerManager().TimerExists(DoublePointsTimerHandle))
		{
			GetWorld() -> GetTimerManager().ClearTimer(DoublePointsTimerHandle);
		}

		GetWorld() -> GetTimerManager().SetTimer(DoublePointsTimerHandle, this, &ThisClass::DoublePointsBuffTimer, 1.f, true, 0.f);	

	}
}

void UBNBEffectHandlerComponent::DoublePointsBuffTimer()
{
	DoublePointsTimer--;

	if (DoublePointsTimer == 0.f)
	{
		if(IsValid(BNBPlayerController))
		{
			BNBPlayerController -> SetIsDoublePoints(false);
			BNBPlayerController -> CallShowEffectText(EBNBEffectType::ET_DoublePoints, false);
			
			if(IsValid(GetWorld()))
			{
				GetWorld() -> GetTimerManager().ClearTimer(DoublePointsTimerHandle);
			}
		}
	}
}

void UBNBEffectHandlerComponent::CreateIncreaseBallSpeedDebuffTimer()
{
	IncreasedBallSpeedTimer = IncreasedBallSpeedDuration;
	
	if(IsValid(GetWorld()))
	{
		if (GetWorld() -> GetTimerManager().TimerExists(IncreasedBallSpeedTimerHandle))
		{
			GetWorld() -> GetTimerManager().ClearTimer(IncreasedBallSpeedTimerHandle);
		}

		GetWorld() -> GetTimerManager().SetTimer(IncreasedBallSpeedTimerHandle, this, &ThisClass::IncreaseBallSpeedDebuffTimer, 1.f, true, 0.f);
	}
}

void UBNBEffectHandlerComponent::IncreaseBallSpeedDebuffTimer()
{
	IncreasedBallSpeedTimer--;

	if (IncreasedBallSpeedTimer == 0.f)
	{
		if(IsValid(MainBall) && IsValid(BNBPlayerController))
		{
			MainBall -> SetIsSpeedIncreased(false);
			MainBall -> SetCurrentBallSpeed(MainBall -> GetBaseBallSpeed());
			BNBPlayerController -> CallShowEffectText(EBNBEffectType::ET_IncreaseBallSpeed, false);
			
			if(IsValid(GetWorld()))
			{
				GetWorld() -> GetTimerManager().ClearTimer(IncreasedBallSpeedTimerHandle);
			}
		}
	}
}

void UBNBEffectHandlerComponent::CreateDecreasePaddleSpeedDebuffTimer()
{
	DecreasePaddleSpeedTimer = DecreasePaddleSpeedDuration;

	if(IsValid(GetWorld()))
	{
		if (GetWorld() -> GetTimerManager().TimerExists(DecreasePaddleSpeedTimerHandle))
		{
			GetWorld() -> GetTimerManager().ClearTimer(DecreasePaddleSpeedTimerHandle);
		}

		GetWorld() -> GetTimerManager().SetTimer(DecreasePaddleSpeedTimerHandle, this, &ThisClass::DecreasePaddleSpeedDebuffTimer, 1.f, true, 0.f);
	}
}

void UBNBEffectHandlerComponent::DecreasePaddleSpeedDebuffTimer()
{
	DecreasePaddleSpeedTimer--;

	if (DecreasePaddleSpeedTimer == 0.f)
	{
		if(IsValid(PlayerPawn) && IsValid(BNBPlayerController))
		{
			PlayerPawn -> SetCurrentSpeedToBase();
			BNBPlayerController -> CallShowEffectText(EBNBEffectType::ET_DecreasePaddleSpeed, false);
			
			if(IsValid(GetWorld()))
			{
				GetWorld() -> GetTimerManager().ClearTimer(DecreasePaddleSpeedTimerHandle);
			}
		}
	}
}
