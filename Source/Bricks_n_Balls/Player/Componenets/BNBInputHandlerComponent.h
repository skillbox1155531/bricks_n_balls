// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "InputMappingContext.h"
#include "Components/ActorComponent.h"
#include "BNBInputHandlerComponent.generated.h"

class UNiagaraComponent;
class ABNBPlayerController;
class ABNBPawn;
class ABNBMainBall;
class UInputMappingContext;
class UInputAction;

struct FInputActionValue;

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class BRICKS_N_BALLS_API UBNBInputHandlerComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UBNBInputHandlerComponent();

private:
	void InitializeInput(ABNBPlayerController* PlayerController);
	void AddInputContext(ABNBPlayerController* PlayerController) const;
	void BindInputEvents(ABNBPlayerController* PlayerController);
	void PlayTrailEffect(const float& Input);
	void HandleHorizontalInput(const FInputActionValue& Value);
	void HandleStopHorizontalInput();
	void HandleGameMenuInput();
	void HandleStartPlay();
protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:
	
	FORCEINLINE void SetMainBall(ABNBMainBall* _MainBall)
	{
		if (_MainBall)
		{
			MainBall = _MainBall;
		}
	}

	FORCEINLINE FKey GetStartKey()
	{
		if (BaseContext)
		{
			TArray Array = BaseContext -> GetMappings();

			for (auto EnhancedActionKeyMapping : Array)
			{
				if (EnhancedActionKeyMapping.Action == IA_Start)
				{
					return EnhancedActionKeyMapping.Key;
				}
			}
		}

		return FKey();
	}

	FORCEINLINE void SetIsCompleted(const bool& _bIsCompleted) { bIsCompleted = _bIsCompleted; }
	
private:
	UPROPERTY()
	ABNBPlayerController* BNBPlayerController;
	UPROPERTY()
	ABNBPawn* PlayerPawn;
	UPROPERTY(VisibleAnywhere)
	ABNBMainBall* MainBall;
	UPROPERTY()
	bool bIsCompleted;
	UPROPERTY(VisibleDefaultsOnly, Category="Input")
	TSoftObjectPtr<UInputMappingContext> BaseContext;
	const FString BaseContextPath = TEXT("/Script/EnhancedInput.InputMappingContext'/Game/Input/Context/IMC_Base.IMC_Base'");
	UPROPERTY(EditDefaultsOnly, Category="Input")
	int32 ContextPriority;
	UPROPERTY(VisibleDefaultsOnly, Category="Input")
	UInputAction* IA_Horizontal;
	const FString IA_HorizontalPath = TEXT("/Script/EnhancedInput.InputAction'/Game/Input/InputActions/IA_Horizontal.IA_Horizontal'");
	UPROPERTY(VisibleDefaultsOnly, Category="Input")
	UInputAction* IA_Start;
	const FString IA_StartPath = TEXT("/Script/EnhancedInput.InputAction'/Game/Input/InputActions/IA_Start.IA_Start'");
	UPROPERTY(VisibleDefaultsOnly, Category="Input")
	UInputAction* IA_GameMenu;
	const FString IA_GameMenuPath = TEXT("/Script/EnhancedInput.InputAction'/Game/Input/InputActions/IA_GameMenu.IA_GameMenu'");
};
