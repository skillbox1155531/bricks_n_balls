// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "BNBEffectHandlerComponent.generated.h"

class ABNBPlayerController;
class ABNBPawn;
class ABNBMainBall;
class ABNBBrickSpawner;

struct FBNBEffectData;
enum class EBNBEffectType : uint8;

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class BRICKS_N_BALLS_API UBNBEffectHandlerComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UBNBEffectHandlerComponent();

private:
	void HandleEffect(const FBNBEffectData& EffectData);
	void ExtendPaddleSize(const float& Value);
	void ShrinkPaddleSize(const float& Value);
	void ExtendMainBallSize(const float& Value);
	void ShrinkMainBallSize(const float& Value);
	void EnableFireball(const EBNBEffectType& EffectData, const float& Value);
	void EnableDoublePoints(const EBNBEffectType& EffectData, const float& Value);
	void AddLives(const float& Value);
	void DecreasePaddleSpeed(const EBNBEffectType& EffectData, const float& Value, const float& DurationValue);
	void IncreaseMainBallSpeed(const EBNBEffectType& EffectData, const float& Value, const float& DurationValue);
	void CallEnableBonusBalls();
	void CreateFireballBuffTimer();
	void FireballBuffTimer();
	void CreateDoublePointsBuffTimer();
	void DoublePointsBuffTimer();
	void CreateIncreaseBallSpeedDebuffTimer();
	void IncreaseBallSpeedDebuffTimer();
	void CreateDecreasePaddleSpeedDebuffTimer();
	void DecreasePaddleSpeedDebuffTimer();
	
protected:
	// Called when the game starts
	virtual void BeginPlay() override;
public:
	FORCEINLINE void SetMainBall(ABNBMainBall* _MainBall) { MainBall = _MainBall; }
private:
	UPROPERTY()
	ABNBPlayerController* BNBPlayerController;
	UPROPERTY()
	ABNBPawn* PlayerPawn;
	UPROPERTY()
	ABNBMainBall* MainBall;
	UPROPERTY()
	ABNBBrickSpawner* BrickSpawner;

	//Fireball
	FTimerHandle FireballTimerHandle;
	UPROPERTY(VisibleAnywhere)
	float FireballTimer;
	float FireballDuration;

	//Double points
	FTimerHandle DoublePointsTimerHandle;
	UPROPERTY(VisibleAnywhere)
	float DoublePointsTimer;
	float DoublePointsDuration;

	//Increase Ball Speed
	FTimerHandle IncreasedBallSpeedTimerHandle;
	UPROPERTY(VisibleAnywhere)
	float IncreasedBallSpeedTimer;
	float IncreasedBallSpeedDuration;

	//Decrease Puddle Speed
	FTimerHandle DecreasePaddleSpeedTimerHandle;
	UPROPERTY(VisibleAnywhere)
	float DecreasePaddleSpeedTimer;
	float DecreasePaddleSpeedDuration;
};