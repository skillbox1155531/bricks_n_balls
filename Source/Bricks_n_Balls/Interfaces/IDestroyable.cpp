// Fill out your copyright notice in the Description page of Project Settings.


#include "IDestroyable.h"

// Add default functionality here for any IIDestroyable functions that are not pure virtual.
void IDestroyable::SetActive(AActor* Actor, const bool& Value)
{
	bIsEnabled = Value;
	Actor -> SetActorTickEnabled(Value);
	Actor ->SetActorHiddenInGame(!Value);
	Actor ->SetActorEnableCollision(Value);
}
