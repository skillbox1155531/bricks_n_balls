// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "IDestroyable.generated.h"

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class UDestroyable : public UInterface
{
	GENERATED_BODY()
};


class BRICKS_N_BALLS_API IDestroyable
{
	GENERATED_BODY()
	
	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:
	virtual void SetActive(AActor* Actor, const bool& Value);
	FORCEINLINE bool& GetIsEnabled() { return bIsEnabled; }
private:
	bool bIsEnabled = true;
};
