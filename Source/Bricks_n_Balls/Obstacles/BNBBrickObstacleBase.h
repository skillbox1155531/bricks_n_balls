// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Bricks_n_Balls/Bricks/BNBBrickBase.h"
#include "BNBBrickObstacleBase.generated.h"

UCLASS()
class BRICKS_N_BALLS_API ABNBBrickObstacleBase : public ABNBStaticObject
{
	GENERATED_BODY()

public:
	ABNBBrickObstacleBase();
	FORCEINLINE void SetStartPoint(const FVector& _StartPoint) { StartPoint = _StartPoint; }
	FORCEINLINE void SetEndPoint(const FVector& _EndPoint) { EndPoint = _EndPoint; }
	FORCEINLINE void SetTargetPoint(const FVector& _TargetPoint) { TargetPoint = _TargetPoint; }
private:
	virtual void HandleHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit) override;
protected:
	UPROPERTY(EditDefaultsOnly)
	UNiagaraSystem* Impact;
	UPROPERTY(VisibleAnywhere)
	FVector StartPoint;
	UPROPERTY(VisibleAnywhere)
	FVector EndPoint;
	UPROPERTY(VisibleAnywhere)
	FVector TargetPoint;
};
