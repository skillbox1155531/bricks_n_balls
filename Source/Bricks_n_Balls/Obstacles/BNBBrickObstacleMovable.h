// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BNBBrickObstacleBase.h"
#include "BNBBrickObstacleMovable.generated.h"

/**
 * 
 */
UCLASS()
class BRICKS_N_BALLS_API ABNBBrickObstacleMovable : public ABNBBrickObstacleBase
{
	GENERATED_BODY()

private:
	virtual void Tick(float DeltaSeconds) override;
	virtual void BeginPlay() override;

	void Move(const float DeltaSeconds);
	
	UPROPERTY(EditDefaultsOnly)
	float InterpSpeed;
	bool bIsInPlace;
};
