// Fill out your copyright notice in the Description page of Project Settings.


#include "BNBBrickObstacleBase.h"
#include "NiagaraFunctionLibrary.h"

ABNBBrickObstacleBase::ABNBBrickObstacleBase()
{
	PrimaryActorTick.bCanEverTick = true;
}

void ABNBBrickObstacleBase::HandleHit(UPrimitiveComponent* HitComponent, AActor* OtherActor,
                                      UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	if (Impact)
	{
		if (MeshComponent)
		{
			UNiagaraFunctionLibrary::SpawnSystemAtLocation(GetWorld(), Impact,
														   MeshComponent->GetComponentLocation(),
														   MeshComponent->GetComponentRotation());
		}
	}
}
