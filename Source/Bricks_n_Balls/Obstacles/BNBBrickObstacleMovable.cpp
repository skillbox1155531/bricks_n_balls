// Fill out your copyright notice in the Description page of Project Settings.

#include "BNBBrickObstacleMovable.h"

void ABNBBrickObstacleMovable::BeginPlay()
{
	Super::BeginPlay();
}

void ABNBBrickObstacleMovable::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	Move(DeltaSeconds);
}

void ABNBBrickObstacleMovable::Move(const float DeltaSeconds)
{
		FVector Lerp = FMath::VInterpConstantTo(GetActorLocation(), TargetPoint, DeltaSeconds, InterpSpeed);
		SetActorLocation(Lerp);

		if (GetActorLocation().Equals(TargetPoint, 0.5f))
		{
			TargetPoint == StartPoint ? TargetPoint = EndPoint : TargetPoint = StartPoint;
		}
}