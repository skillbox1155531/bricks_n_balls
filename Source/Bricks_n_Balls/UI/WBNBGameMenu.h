// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "WBNBGameMenu.generated.h"

class ABNBPlayerController;
class UWBNBButton;

UCLASS()
class BRICKS_N_BALLS_API UWBNBGameMenu : public UUserWidget
{
	GENERATED_BODY()

public:
	void Initialization(ABNBPlayerController* _PlayerController);

private:
	void HandleButtonClicked(const UWBNBButton* Button);
	void StartManualGame();
	void StartAutoGame();
	void ResumeGame();
	void ExitGame();

protected:
	virtual void NativeConstruct() override;
	
private:
	UPROPERTY()
	ABNBPlayerController* PlayerController;
	UPROPERTY(meta=(BindWidget))
	UWBNBButton* ManualGameButton;
	UPROPERTY(meta=(BindWidget))
	UWBNBButton* AutoGameButton;
	UPROPERTY(meta=(BindWidget))
	UWBNBButton* ResumeGameButton;
	UPROPERTY(meta=(BindWidget))
	UWBNBButton* ExitButton;
};
