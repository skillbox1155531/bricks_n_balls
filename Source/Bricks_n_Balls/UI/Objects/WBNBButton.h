// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Bricks_n_Balls/Helper/DelegatesHolder.h"
#include "WBNBButton.generated.h"

class UButton;
class UTextBlock;

UCLASS()
class BRICKS_N_BALLS_API UWBNBButton : public UUserWidget
{
	GENERATED_BODY()

public:
	FOnButtonClicked OnButtonClicked;
	
private:
	UFUNCTION()
	void OnButtonClickedDelegate();
	
protected:
	virtual void NativePreConstruct() override;
	virtual void NativeConstruct() override;
	
private:
	UPROPERTY(meta=(BindWidget))
	UButton* MainButton;
	UPROPERTY(meta=(BindWidget))
	UTextBlock* MainButtonText;
	UPROPERTY(EditAnywhere)
	FText Text;
};
