// Fill out your copyright notice in the Description page of Project Settings.


#include "WBNBButton.h"
#include "Components/Button.h"
#include "Components/TextBlock.h"

void UWBNBButton::NativePreConstruct()
{
	Super::NativePreConstruct();

	if (IsValid(MainButtonText))
	{
		MainButtonText -> SetText(Text);
	}
}

void UWBNBButton::NativeConstruct()
{
	Super::NativeConstruct();

	if (IsValid(MainButton))
	{
		MainButton -> OnClicked.AddUniqueDynamic(this, &ThisClass::OnButtonClickedDelegate);
	}
}

void UWBNBButton::OnButtonClickedDelegate()
{
	OnButtonClicked.Broadcast(this);
}