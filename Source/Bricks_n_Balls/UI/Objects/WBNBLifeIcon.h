// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "WBNBLifeIcon.generated.h"

class UImage;

UCLASS()
class BRICKS_N_BALLS_API UWBNBLifeIcon : public UUserWidget
{
	GENERATED_BODY()

public:
	FORCEINLINE bool GetIsActive() const { return bIsActive; }
	FORCEINLINE void SetIsActive(const bool& Value) { bIsActive = Value; }

private:
	UPROPERTY(meta=(BindWidget))
	UImage* Icon;
	UPROPERTY(VisibleAnywhere)
	bool bIsActive;
};
