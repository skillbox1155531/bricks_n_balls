// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "WBNBEndLevelScreen.generated.h"

class UTextBlock;

UCLASS()
class BRICKS_N_BALLS_API UWBNBEndLevelScreen : public UUserWidget
{
	GENERATED_BODY()

public:
	void ShowScreen(const bool& bIsWin);
private:
	FString GetRandomGameOverPhrase();
	FString GetRandomWinPhrase();
private:
	UPROPERTY(meta=(BindWidget))
	UTextBlock* StateTextBlock;
	UPROPERTY(meta=(BindWidget))
	UTextBlock* PhraseTextBlock;
	
	const FString GameOver = TEXT("Game Over");
	const FString Win = TEXT("Level Completed");

	UPROPERTY(EditDefaultsOnly)
	TArray<FString> GameOverPhrases;
	UPROPERTY(EditDefaultsOnly)
	TArray<FString> WinPhrases;
};


