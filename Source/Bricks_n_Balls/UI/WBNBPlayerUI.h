// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Bricks_n_Balls/Structs/BNBEffectData.h"
#include "WBNBPlayerUI.generated.h"

class UTextBlock;
class UWBNBLifeIcon;

enum class EBNBEffectType : uint8;

UCLASS()
class BRICKS_N_BALLS_API UWBNBPlayerUI : public UUserWidget
{
	GENERATED_BODY()

public:
	void InitializeLives();
	void IncreaseLives(const int32& Value);
	void DecreaseLives();
	void SetScore(const int32& Value);
	void SetRecord(const int32& Value);
	void SetEffectVisibility(const EBNBEffectType& EffectType, const bool& Value);
	void SetStartButton(const FKey& Value);
	void SetStartTextBlockVisibility(const bool& Value);
	void SetRecordScoreBlockVisibility(const bool& Value);
	void SetTextBlockVisibility(UTextBlock* SettTextBlockVisibility, const bool& Value);

protected:
	virtual void NativeOnInitialized() override;
private:
	UPROPERTY(meta=(BindWidget))
	UTextBlock* LivesTextBlock;
	UPROPERTY(meta=(BindWidget))
	UTextBlock* CurrentScoreTextBlock;
	UPROPERTY(meta=(BindWidget))
	UTextBlock* RecordScoreTextBlock;
	UPROPERTY(meta=(BindWidget))
	UTextBlock* DoublePointsTextBlock;
	UPROPERTY(meta=(BindWidget))
	UTextBlock* FireballTextBlock;
	UPROPERTY(meta=(BindWidget))
	UTextBlock* FastBallTextBlock;
	UPROPERTY(meta=(BindWidget))
	UTextBlock* SlowPaddleTextBlock;
	UPROPERTY(meta=(BindWidget))
	UTextBlock* StartGameTextBlock;
	UPROPERTY()
	TArray<UWBNBLifeIcon*> LivesArr;
};
