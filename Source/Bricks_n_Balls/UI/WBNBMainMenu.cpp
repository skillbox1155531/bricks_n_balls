// Fill out your copyright notice in the Description page of Project Settings.


#include "WBNBMainMenu.h"
#include "Bricks_n_Balls/Player/BNBPlayerController.h"
#include "Bricks_n_Balls/UI/Objects/WBNBButton.h"

void UWBNBMainMenu::NativeConstruct()
{
	Super::NativeConstruct();

	if (IsValid(ManualGameButton))
	{
		ManualGameButton -> OnButtonClicked.AddUObject(this, &ThisClass::HandleButtonClicked);
	}
	if (IsValid(AutoGameButton))
	{
		AutoGameButton -> OnButtonClicked.AddUObject(this, &ThisClass::HandleButtonClicked);
	}
	if (IsValid(ExitButton))
	{
		ExitButton -> OnButtonClicked.AddUObject(this, &ThisClass::HandleButtonClicked);
	}
}


void UWBNBMainMenu::Initialization(ABNBPlayerController* _PlayerController)
{
	if (_PlayerController)
	{
		PlayerController = _PlayerController;
	}
}

void UWBNBMainMenu::HandleButtonClicked(const UWBNBButton* Button)
{
	if (Button == ManualGameButton)
	{
		StartManualGame();
	}
	else if(Button == AutoGameButton)
	{
		StartAutoGame();
	}
	else if(Button == ExitButton)
	{
		ExitGame();
	}
}


void UWBNBMainMenu::StartManualGame()
{
	if (IsValid(PlayerController))
	{
		SetVisibility(ESlateVisibility::Hidden);
		PlayerController -> CallStartGame(true);
	}
}

void UWBNBMainMenu::StartAutoGame()
{
	if (IsValid(PlayerController))
	{
		SetVisibility(ESlateVisibility::Hidden);
		PlayerController -> CallStartGame(false);
	}
}

void UWBNBMainMenu::ExitGame()
{
	if (IsValid(PlayerController))
	{
		PlayerController -> CallExitGame();
	}
}