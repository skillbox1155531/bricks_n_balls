// Fill out your copyright notice in the Description page of Project Settings.


#include "WBNBGameMenu.h"
#include "Bricks_n_Balls/UI/Objects/WBNBButton.h"
#include "Bricks_n_Balls/Player/BNBPlayerController.h"

void UWBNBGameMenu::NativeConstruct()
{
	Super::NativeConstruct();
	
	if (IsValid(ManualGameButton))
	{
		ManualGameButton -> OnButtonClicked.AddUObject(this, &ThisClass::HandleButtonClicked);
	}
	if (IsValid(AutoGameButton))
	{
		AutoGameButton -> OnButtonClicked.AddUObject(this, &ThisClass::HandleButtonClicked);
	}
	if (IsValid(ResumeGameButton))
	{
		ResumeGameButton -> OnButtonClicked.AddUObject(this, &ThisClass::HandleButtonClicked);
	}
	if (IsValid(ExitButton))
	{
		ExitButton -> OnButtonClicked.AddUObject(this, &ThisClass::HandleButtonClicked);
	}
}

void UWBNBGameMenu::Initialization(ABNBPlayerController* _PlayerController)
{
	if (_PlayerController)
	{
		PlayerController = _PlayerController;
	}
}

void UWBNBGameMenu::HandleButtonClicked(const UWBNBButton* Button)
{
	if (Button == ManualGameButton)
	{
		StartManualGame();
	}
	else if(Button == AutoGameButton)
	{
		StartAutoGame();
	}
	else if(Button == ResumeGameButton)
	{
		ResumeGame();
	}
	else if(Button == ExitButton)
	{
		ExitGame();
	}
}

void UWBNBGameMenu::StartManualGame()
{
	if (IsValid(PlayerController))
	{
		SetVisibility(ESlateVisibility::Hidden);
		PlayerController -> CallStartNewGame(true);
	}
}

void UWBNBGameMenu::StartAutoGame()
{
	if (IsValid(PlayerController))
	{
		SetVisibility(ESlateVisibility::Hidden);
		PlayerController -> CallStartNewGame(false);
	}
}

void UWBNBGameMenu::ResumeGame()
{
	if (IsValid(PlayerController))
	{
		SetVisibility(ESlateVisibility::Hidden);
		PlayerController -> CallResumeGame();
	}
}

void UWBNBGameMenu::ExitGame()
{
	if (IsValid(PlayerController))
	{
		PlayerController -> CallExitGame();
	}
}