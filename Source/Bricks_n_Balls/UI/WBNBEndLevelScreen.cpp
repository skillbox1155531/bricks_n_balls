// Fill out your copyright notice in the Description page of Project Settings.

#include "WBNBEndLevelScreen.h"
#include "Components/TextBlock.h"

void UWBNBEndLevelScreen::ShowScreen(const bool& bIsWin)
{
	if (bIsWin)
	{
		if (IsValid(StateTextBlock) && IsValid(PhraseTextBlock))
		{
			StateTextBlock -> SetText(FText::FromString(Win));
			FString String = GetRandomWinPhrase();
			String = String.Replace(TEXT(". "), TEXT(".\n"));
			String = String.Replace(TEXT("$"), TEXT("\n"));
			String = String.Replace(TEXT(", "), TEXT(",\n"));
			PhraseTextBlock -> SetText(FText::FromString(String));
		}
	}
	else
	{
		if (IsValid(StateTextBlock) && IsValid(PhraseTextBlock))
		{
			StateTextBlock -> SetText(FText::FromString(GameOver));
			
			FString String = GetRandomGameOverPhrase();
			String = String.Replace(TEXT(". "), TEXT(".\n"));
			String = String.Replace(TEXT("$"), TEXT("\n"));
			String = String.Replace(TEXT(", "), TEXT(",\n"));
			PhraseTextBlock -> SetText(FText::FromString(String));
		}
	}
}

FString UWBNBEndLevelScreen::GetRandomGameOverPhrase()
{
	if (GameOverPhrases.Num() != 0)
	{
		const int Index = FMath::RandRange(0, GameOverPhrases.Num() - 1);

		return GameOverPhrases[Index];
	}

	return TEXT("-1");
}

FString UWBNBEndLevelScreen::GetRandomWinPhrase()
{
	if (WinPhrases.Num() != 0)
	{
		const int Index = FMath::RandRange(0, WinPhrases.Num() - 1);

		return WinPhrases[Index];
	}

	return TEXT("-1");
}
