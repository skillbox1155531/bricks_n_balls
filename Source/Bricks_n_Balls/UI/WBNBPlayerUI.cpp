// Fill out your copyright notice in the Description page of Project Settings.


#include "WBNBPlayerUI.h"
#include "Bricks_n_Balls/UI/Objects/WBNBLifeIcon.h"
#include "Blueprint/WidgetTree.h"
#include "Components/TextBlock.h"

void UWBNBPlayerUI::NativeOnInitialized()
{
	Super::NativeOnInitialized();

	InitializeLives();
}

void UWBNBPlayerUI::InitializeLives()
{
	TArray<UWidget*> Widgets;
	
	this -> WidgetTree -> GetAllWidgets(Widgets);

	for (UWidget* Widget : Widgets)
	{
		if (UWBNBLifeIcon* Icon = Cast<UWBNBLifeIcon>(Widget))
		{
			LivesArr.Emplace(Icon);
		}
	}
}

void UWBNBPlayerUI::IncreaseLives(const int32& Value)
{
	for (int i = 0; i <= Value - 1; ++i)
	{
		if (LivesArr.IsValidIndex(i) && !LivesArr[i] -> GetIsActive())
		{
			LivesArr[i] -> SetIsActive(true);
			LivesArr[i] -> SetVisibility(ESlateVisibility::Visible);
		}
	}
}

void UWBNBPlayerUI::DecreaseLives()
{
	for (int i = LivesArr.Num() - 1; i >= 0; --i)
	{
		if (LivesArr.IsValidIndex(i) && LivesArr[i] -> GetIsActive())
		{
			LivesArr[i] -> SetIsActive(false);
			LivesArr[i] -> SetVisibility(ESlateVisibility::Hidden);
			return;
		}
	}
}

void UWBNBPlayerUI::SetScore(const int32& Value)
{
	if (CurrentScoreTextBlock)
	{
		const FText FormatPattern = FText::FromString("Score: {0}");
		const FText FormattedText = FText::Format(FormatPattern, FText::AsNumber(Value));

		CurrentScoreTextBlock -> SetText(FormattedText);
	}
}

void UWBNBPlayerUI::SetRecord(const int32& Value)
{
	if (RecordScoreTextBlock)
	{
		const FText FormatPattern = FText::FromString("Record Score: {0}");
		const FText FormattedText = FText::Format(FormatPattern, FText::AsNumber(Value));

		RecordScoreTextBlock -> SetText(FormattedText);
	}
}

void UWBNBPlayerUI::SetEffectVisibility(const EBNBEffectType& EffectType, const bool& Value)
{
	if (EffectType == EBNBEffectType::ET_DoublePoints)
	{
		SetTextBlockVisibility(DoublePointsTextBlock, Value);
	}
	else if (EffectType == EBNBEffectType::ET_Fireball)
	{
		SetTextBlockVisibility(FireballTextBlock, Value);
	}
	else if (EffectType == EBNBEffectType::ET_IncreaseBallSpeed)
	{
		SetTextBlockVisibility(FastBallTextBlock, Value);
	}
	else if (EffectType == EBNBEffectType::ET_DecreasePaddleSpeed)
	{
		SetTextBlockVisibility(SlowPaddleTextBlock, Value);
	}
}

void UWBNBPlayerUI::SetStartButton(const FKey& Value)
{
	if (StartGameTextBlock)
	{
		const FText FormatPattern = FText::FromString("Press {0} to Start a Game");
		const FText FormattedText = FText::Format(FormatPattern, FText::FromString(Value.ToString()));

		StartGameTextBlock -> SetText(FormattedText);
	}
}

void UWBNBPlayerUI::SetStartTextBlockVisibility(const bool& Value)
{
	SetTextBlockVisibility(StartGameTextBlock, Value);
}

void UWBNBPlayerUI::SetRecordScoreBlockVisibility(const bool& Value)
{
	SetTextBlockVisibility(RecordScoreTextBlock, Value);
}

void UWBNBPlayerUI::SetTextBlockVisibility(UTextBlock* TextBlock, const bool& Value)
{
	if (TextBlock)
	{
		Value
			? TextBlock->SetVisibility(ESlateVisibility::Visible)
			: TextBlock->SetVisibility(ESlateVisibility::Hidden);
	}
}
