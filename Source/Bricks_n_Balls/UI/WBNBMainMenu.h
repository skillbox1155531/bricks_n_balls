// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "WBNBMainMenu.generated.h"

class UWBNBButton;
class ABNBPlayerController;

UCLASS()
class BRICKS_N_BALLS_API UWBNBMainMenu : public UUserWidget
{
	GENERATED_BODY()

public:
	void Initialization(ABNBPlayerController* _PlayerController);

private:
	void HandleButtonClicked(const UWBNBButton* Button);
	void StartManualGame();
	void StartAutoGame();
	void ExitGame();

protected:
	virtual void NativeConstruct() override;
	
private:
	UPROPERTY()
	ABNBPlayerController* PlayerController;
	UPROPERTY(meta=(BindWidget))
	UWBNBButton* ManualGameButton;
	UPROPERTY(meta=(BindWidget))
	UWBNBButton* AutoGameButton;
	UPROPERTY(meta=(BindWidget))
	UWBNBButton* ExitButton;
};
