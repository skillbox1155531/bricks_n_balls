// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Bricks_n_Balls/Bricks/BNBBrickBase.h"
#include "BNBMediumBrick.generated.h"

/**
 * 
 */
UCLASS()
class BRICKS_N_BALLS_API ABNBMediumBrick : public ABNBBrickBase
{
	GENERATED_BODY()

	ABNBMediumBrick();
};
