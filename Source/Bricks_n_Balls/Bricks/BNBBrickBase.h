// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Bricks_n_Balls/Objects/BNBStaticObject.h"
#include "GameFramework/Actor.h"
#include "BNBBrickBase.generated.h"

class ABNBBrickSpawner;
class UNiagaraSystem;
class UMaterialInstance;
class ABNBEffectBase;

UCLASS()
class BRICKS_N_BALLS_API ABNBBrickBase : public ABNBStaticObject
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABNBBrickBase();

private:
	virtual void HandleOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult) override;
	virtual void HandleHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit) override;
	void SetStartMaterial();
	void SetHitMaterial();
	void SpawnImpact();
	void Deactivate();
	
protected:
	virtual void BeginPlay() override;
	
public:
	void EnableCollision(const bool& Value);
	void SetEffectPtr(ABNBEffectBase* _EffectPtr) { EffectPtr = _EffectPtr; }
	void SetBrickSpawner(ABNBBrickSpawner* _BrickSpawner) { BrickSpawner = _BrickSpawner; }
	
private:
protected:
	UPROPERTY(EditDefaultsOnly)
	UNiagaraSystem* Impact;
	UPROPERTY()
	ABNBEffectBase* EffectPtr;
	UPROPERTY()
	ABNBBrickSpawner* BrickSpawner;
	
	UPROPERTY(EditDefaultsOnly)
	int32 CurrentHits;
	UPROPERTY(EditDefaultsOnly)
	int32 MaxHits;
	UPROPERTY(EditDefaultsOnly)
	int32 PointsAmount;
	UPROPERTY(EditDefaultsOnly)
	TArray<UMaterialInstance*> MaterialsArr;
	UPROPERTY(EditDefaultsOnly)
	bool bIsInvincible;
};
