// Fill out your copyright notice in the Description page of Project Settings.


#include "BNBBrickBase.h"
#include "NiagaraFunctionLibrary.h"
#include "Bricks_n_Balls/Ball/BNBMainBall.h"
#include "Bricks_n_Balls/Effects/BNBEffectBase.h"
#include "Bricks_n_Balls/Spawners/BNBBrickSpawner.h"
#include "Components/BoxComponent.h"

// Sets default values
ABNBBrickBase::ABNBBrickBase()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	Impact = nullptr;
	EffectPtr = nullptr;
	CurrentHits = 0;
	MaxHits = 0;
	PointsAmount = 0;
	bIsInvincible = false;
}

void ABNBBrickBase::BeginPlay()
{
	Super::BeginPlay();

	SetStartMaterial();
}

void ABNBBrickBase::HandleOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
                                  UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep,
                                  const FHitResult& SweepResult)
{
	if (auto Ball = Cast<ABNBMainBall>(OtherActor))
	{
		if (Ball -> GetIsFireball())
		{
			SpawnImpact();
			CurrentHits = MaxHits;
			Deactivate();
		}
	}
	
}

void ABNBBrickBase::HandleHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp,
							  FVector NormalImpulse, const FHitResult& Hit)
{
	SpawnImpact();
	SetHitMaterial();
	Deactivate();
}

void ABNBBrickBase::SetStartMaterial()
{
	if (MaterialsArr.Num() != 0)
	{
		if (MeshComponent)
		{
			MeshComponent->SetMaterial(0, MaterialsArr[CurrentHits]);
		}
	}
}

void ABNBBrickBase::SetHitMaterial()
{
	CurrentHits++;

	if (MaterialsArr.IsValidIndex(CurrentHits))
	{
		if (MeshComponent)
		{
			MeshComponent->SetMaterial(0, MaterialsArr[CurrentHits]);
		}
	}
}

void ABNBBrickBase::SpawnImpact()
{
	if (Impact)
	{
		if (MeshComponent)
		{
			UNiagaraFunctionLibrary::SpawnSystemAtLocation(GetWorld(), Impact,
			                                               MeshComponent->GetComponentLocation(),
			                                               MeshComponent->GetComponentRotation());
		}
	}
}

void ABNBBrickBase::Deactivate()
{
	if (CurrentHits == MaxHits)
	{
		SetActive(this, false);
		BrickSpawner -> BrickDestroyed(PointsAmount);
		
		if (EffectPtr)
		{
			EffectPtr->SetActive(EffectPtr, true);
		}
	}
}

void ABNBBrickBase::EnableCollision(const bool& Value)
{
	if (BoxComponent)
	{
		if (Value)
		{
			BoxComponent->SetCollisionResponseToChannel(ECC_GameTraceChannel2, ECR_Block);
		}
		else
		{
			BoxComponent->SetCollisionResponseToChannel(ECC_GameTraceChannel2, ECR_Overlap);
		}
	}
}
