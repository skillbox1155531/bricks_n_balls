// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Bricks_n_Balls/Bricks/BNBBrickBase.h"
#include "BNBNormalBrick.generated.h"

/**
 * 
 */
UCLASS()
class BRICKS_N_BALLS_API ABNBNormalBrick : public ABNBBrickBase
{
	GENERATED_BODY()

	ABNBNormalBrick();
};
