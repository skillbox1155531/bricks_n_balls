// Fill out your copyright notice in the Description page of Project Settings.


#include "BNBStaticObject.h"
#include "Bricks_n_Balls/Ball/BNBBallBase.h"
#include "Components/BoxComponent.h"

// Sets default values
ABNBStaticObject::ABNBStaticObject()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	BoxComponent = CreateDefaultSubobject<UBoxComponent>("Box");
	RootComponent = BoxComponent;
	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>("Mesh");
	MeshComponent -> SetupAttachment(BoxComponent);

	bIsHit = false;
	bIsVulnerable = false;
}

void ABNBStaticObject::BeginPlay()
{
	Super::BeginPlay();

	if (IsValid(BoxComponent))
	{
		BoxComponent -> OnComponentBeginOverlap.AddDynamic(this, &ThisClass::HandleOverlap);
		BoxComponent -> OnComponentHit.AddDynamic(this, &ThisClass::HandleHit);
	}
}

void ABNBStaticObject::HandleOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
	UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
}

void ABNBStaticObject::HandleHit(UPrimitiveComponent* HitComponent,
	AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
}

void ABNBStaticObject::SetActive(AActor* Actor, const bool& Value)
{
	IDestroyable::SetActive(Actor, Value);
}
