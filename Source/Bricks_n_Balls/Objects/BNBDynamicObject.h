// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Bricks_n_Balls/Interfaces/IDestroyable.h"
#include "GameFramework/Actor.h"
#include "BNBDynamicObject.generated.h"

class USphereComponent;
class UStaticMeshComponent;

class AKillZVolume;
UCLASS()
class BRICKS_N_BALLS_API ABNBDynamicObject : public AActor, public IDestroyable
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABNBDynamicObject();

	virtual void SetActive(AActor* Actor, const bool& Value) override;
protected:
	virtual void BeginPlay() override;
	UFUNCTION()
	virtual void HandleOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
												   UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
	UFUNCTION()
	virtual void HandleHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit);
	virtual void Movement(const float& DeltaTime);
	UPROPERTY(EditDefaultsOnly)
	USphereComponent* Root;
	UPROPERTY(EditDefaultsOnly)
	UStaticMeshComponent* MeshComponent;
private:
	UPROPERTY()
	AKillZVolume* KillZVolume;
};