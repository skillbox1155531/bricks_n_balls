// Fill out your copyright notice in the Description page of Project Settings.

#include "BNBDynamicObject.h"

#include "Components/SphereComponent.h"
#include "Kismet/GameplayStatics.h"
#include "GameFramework/KillZVolume.h"

// Sets default values
ABNBDynamicObject::ABNBDynamicObject()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	Root = CreateDefaultSubobject<USphereComponent>("Root");
	RootComponent = Root;

	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>("Mesh");
	MeshComponent -> SetCollisionEnabled(ECollisionEnabled::NoCollision);
	MeshComponent -> SetGenerateOverlapEvents(false);
	MeshComponent -> AttachToComponent(RootComponent, FAttachmentTransformRules::KeepRelativeTransform);
	
	KillZVolume = nullptr;
}

void ABNBDynamicObject::BeginPlay()
{
	Super::BeginPlay();

	if (IsValid(Root))
	{
		Root -> OnComponentBeginOverlap.AddDynamic(this, &ThisClass::HandleOverlap);
		Root -> OnComponentHit.AddDynamic(this, &ThisClass::HandleHit);
	}
	
	if (IsValid(Cast<AKillZVolume>(UGameplayStatics::GetActorOfClass(this, AKillZVolume::StaticClass()))))
	{
		KillZVolume = Cast<AKillZVolume>(UGameplayStatics::GetActorOfClass(this, AKillZVolume::StaticClass()));
	}
}

void ABNBDynamicObject::SetActive(AActor* Actor, const bool& Value)
{
	IDestroyable::SetActive(Actor, Value);
}

void ABNBDynamicObject::HandleOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
	UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (KillZVolume)
	{
		if (KillZVolume == OtherActor)
		{
			SetActive(this, false);
		}
	}
}

void ABNBDynamicObject::HandleHit(UPrimitiveComponent* HitComponent, AActor* OtherActor,
	UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
}


void ABNBDynamicObject::Movement(const float& DeltaTime)
{
}
