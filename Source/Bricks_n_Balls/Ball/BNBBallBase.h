// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Bricks_n_Balls/Objects/BNBDynamicObject.h"
#include "Bricks_n_Balls/Helper/DelegatesHolder.h"
#include "BNBBallBase.generated.h"

class UStaticMeshComponent;
class UNiagaraSystem;
class ABNBPawn;

UCLASS()
class BRICKS_N_BALLS_API ABNBBallBase : public ABNBDynamicObject
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABNBBallBase();
	virtual void Tick(float DeltaTime) override;

private:
	virtual void Movement(const float& DeltaTime) override;
	virtual void HandleHit(UPrimitiveComponent* HitComponent,
		AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit) override;
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	FOnBallKilled OnBallKilled;

	FORCEINLINE bool GetIsPlaying() const { return bIsPlaying; }
	FORCEINLINE bool GetIsFireball() const { return bIsFireball; }
	FORCEINLINE bool GetIsSpeedIncreased() const { return bIsSpeedIncreased; }
	FORCEINLINE FVector GetCurrentDirection() const { return CurrentDirection; }
	FORCEINLINE float GetBaseBallSpeed() const { return BaseSpeed; }
	
	FORCEINLINE void SetIsPlaying(const bool& _bIsPlaying) { bIsPlaying = _bIsPlaying, GetIsEnabled() = _bIsPlaying; }
	FORCEINLINE void SetIsFireball(const bool& _bIsFireball) { bIsFireball = _bIsFireball; }
	FORCEINLINE void SetIsMainBall(const bool& _bIsMainBall) { bIsMainBall = _bIsMainBall; }
	FORCEINLINE void SetStartPosition(const FVector& _StartPosition) { StartPosition = _StartPosition; }
	FORCEINLINE void SetIsSpeedIncreased(const bool& _bIsSpeedIncreased) { bIsSpeedIncreased = _bIsSpeedIncreased; }
	FORCEINLINE void SetCurrentBallSpeed(const float& BallSpeed) { CurrentSpeed = BallSpeed; }
protected:
	UPROPERTY(EditAnywhere, Category="Settings | Movement")
	bool bIsPlaying;
	UPROPERTY(VisibleAnywhere)
	bool bIsMainBall;
	UPROPERTY(VisibleAnywhere)
	bool bIsFireball;
	UPROPERTY(VisibleAnywhere)
	bool bIsSpeedIncreased;
	UPROPERTY()
	float RandomCoef;
	UPROPERTY(EditAnywhere, Category="Settings | Movement")
	FVector InitialDirection;
	UPROPERTY(EditAnywhere, Category="Settings | Movement")
	FVector CurrentDirection;
	UPROPERTY(VisibleAnywhere, Category="Settings | Movement")
	FVector StartPosition;
	UPROPERTY(EditDefaultsOnly, Category="Settings | Movement")
	float BaseSpeed;
	UPROPERTY(EditAnywhere, Category="Settings | Movement")
	float CurrentSpeed;

};
