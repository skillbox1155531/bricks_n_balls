// Fill out your copyright notice in the Description page of Project Settings.


#include "Bricks_n_Balls/Ball/BNBBonusBall.h"

void ABNBBonusBall::EnableBall(const bool& Value)
{
	SetActive(this, Value);
	SetIsPlaying(Value);
}

void ABNBBonusBall::HandleOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
                                  UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	Super::HandleOverlap(OverlappedComponent, OtherActor, OtherComp, OtherBodyIndex, bFromSweep, SweepResult);

	if (!GetIsEnabled())
	{
		bIsPlaying = GetIsEnabled();
		SetActorLocation(StartPosition);
		CurrentDirection = InitialDirection;
	}
}
