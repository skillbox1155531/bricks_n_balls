// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Bricks_n_Balls/Ball/BNBBallBase.h"
#include "BNBBonusBall.generated.h"

/**
 * 
 */
UCLASS()
class BRICKS_N_BALLS_API ABNBBonusBall : public ABNBBallBase
{
	GENERATED_BODY()

public:
	void EnableBall(const bool& Value);
protected:
	virtual void HandleOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
						   UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult) override;
};