// Fill out your copyright notice in the Description page of Project Settings.


#include "BNBMainBall.h"
#include "NiagaraFunctionLibrary.h"
#include "NiagaraComponent.h"
#include "Components/SphereComponent.h"

ABNBMainBall::ABNBMainBall()
{
	FireballEffectPtr = nullptr;
}

void ABNBMainBall::BeginPlay()
{
	Super::BeginPlay();

	FireballEffectPtr = UNiagaraFunctionLibrary::SpawnSystemAttached(FireballEffect, RootComponent, NAME_None,
			Root->GetComponentLocation(), Root->GetComponentRotation(), EAttachLocation::KeepWorldPosition, true);

	if (IsValid(FireballEffectPtr))
	{
		FireballEffectPtr -> SetVisibility(false);
	}
}

void ABNBMainBall::HandleOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
                                 UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	Super::HandleOverlap(OverlappedComponent, OtherActor, OtherComp, OtherBodyIndex, bFromSweep, SweepResult);
	
	if (!GetIsEnabled())
	{
		bIsPlaying = GetIsEnabled();
		OnBallKilled.Broadcast(bIsPlaying);
		SetActorLocation(StartPosition);
		SetActorRotation(FRotator::ZeroRotator);
		CurrentDirection = InitialDirection;
		SetActive(this, true);
	}
}

void ABNBMainBall::EnableFireball()
{
	if (IsValid(MeshComponent) && IsValid(FireballEffectPtr))
	{
		MeshComponent -> SetVisibility(false);
		FireballEffectPtr -> SetVisibility(true);
		bIsFireball = true;
	}
}

void ABNBMainBall::DisableFireball()
{
	if (IsValid(MeshComponent) && IsValid(FireballEffectPtr))
	{
		MeshComponent -> SetVisibility(true);
		FireballEffectPtr -> SetVisibility(false);
		bIsFireball = false;
	}
}