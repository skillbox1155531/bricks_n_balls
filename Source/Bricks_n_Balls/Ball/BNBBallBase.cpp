// Fill out your copyright notice in the Description page of Project Settings.

#include "BNBBallBase.h"
#include "Bricks_n_Balls/Bricks/BNBBrickBase.h"
#include "Kismet/KismetMathLibrary.h"

// Sets default values
ABNBBallBase::ABNBBallBase()
{
	RandomCoef = 0.05f;
	InitialDirection = FVector(1.f, 1.f, 0.f);
	CurrentDirection = InitialDirection;

	BaseSpeed = 30.f;
	CurrentSpeed = BaseSpeed;

	bIsPlaying = false;
	bIsMainBall = false;
	bIsFireball = false;
}

// Called when the game starts or when spawned
void ABNBBallBase::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void ABNBBallBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	Movement(DeltaTime);
}

void ABNBBallBase::Movement(const float& DeltaTime)
{
	if (bIsPlaying)
	{
		const FVector DeltaLocation = (CurrentSpeed * CurrentDirection) * DeltaTime;
		
		AddActorWorldOffset(DeltaLocation, true);
	}
}

void ABNBBallBase::HandleHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp,
	FVector NormalImpulse, const FHitResult& Hit)
{
	if (Hit.bBlockingHit)
	{
		if (bIsFireball && Cast<ABNBBrickBase>(OtherActor) || Hit.GetActor() == this)
		{
			return;
		}
		
		const FVector HitNormal(Hit.Normal.X, Hit.Normal.Y, 0.f);
		CurrentDirection = UKismetMathLibrary::GetReflectionVector(CurrentDirection, HitNormal);
		
		CurrentDirection += FVector(0.f, FMath::RandRange(-RandomCoef, RandomCoef), 0.f);
		SetActorRotation(CurrentDirection.Rotation());
		
	}
}
