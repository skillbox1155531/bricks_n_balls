// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "CoreMinimal.h"
#include "BNBBallBase.h"
#include "BNBMainBall.generated.h"

class ABNBPawn;
class UNiagaraSystem;
class UNiagaraComponent;

UCLASS()
class BRICKS_N_BALLS_API ABNBMainBall : public ABNBBallBase
{
	GENERATED_BODY()

public:
	ABNBMainBall();
private:

protected:
	virtual void BeginPlay() override;
	virtual void HandleOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
						   UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult) override;
public:
	void EnableFireball();
	void DisableFireball();

	USphereComponent* GetSphereComponent() const { return Root; }
private:
	UPROPERTY(EditDefaultsOnly, Category="Settings | Effect")
	UNiagaraSystem* FireballEffect;
	UPROPERTY()
	UNiagaraComponent* FireballEffectPtr;
protected:
};
