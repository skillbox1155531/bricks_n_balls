﻿#pragma once

#include "Bricks_n_Balls/Enums/BNBEffectType.h"
#include "BNBEffectData.generated.h"

USTRUCT()
struct FBNBEffectData
{
	GENERATED_BODY()

	FBNBEffectData() : EffectType(EBNBEffectType::ET_None), bIsPermanent(true), EffectValue(0.f), EffectDuration(0.f)
	{}

public:
	FORCEINLINE EBNBEffectType GetEffectType() const { return EffectType; }
	FORCEINLINE float GetEffectValue() const { return  EffectValue; }
	FORCEINLINE float GetEffectDuration() const { return EffectDuration; }
	
private:
	UPROPERTY(EditDefaultsOnly)
	EBNBEffectType EffectType;
	UPROPERTY(EditDefaultsOnly)
	bool bIsPermanent;
	UPROPERTY(EditDefaultsOnly)
	float EffectValue;
	UPROPERTY(EditDefaultsOnly, meta=(EditCondition = "!bIsPermanent", EditConditionHides))
	float EffectDuration;
};
