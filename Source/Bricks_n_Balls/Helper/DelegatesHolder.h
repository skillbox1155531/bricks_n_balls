﻿#pragma once

class UWBNBButton;
struct FBNBEffectData;

DECLARE_MULTICAST_DELEGATE_OneParam(FOnBallKilled, const bool&);
DECLARE_MULTICAST_DELEGATE_OneParam(FOnEnableBonusBalls, const bool&)
DECLARE_MULTICAST_DELEGATE_OneParam(FOnEnableBrickCollision, const bool&);
DECLARE_MULTICAST_DELEGATE(FOnDeactivateEffects);
DECLARE_DELEGATE_OneParam(FOnApplyEffect, const FBNBEffectData&);
DECLARE_MULTICAST_DELEGATE_OneParam(FOnButtonClicked, const UWBNBButton*);
