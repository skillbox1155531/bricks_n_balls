﻿#pragma once

UENUM(BlueprintType)
enum class EBNBEffectType : uint8
{
	ET_ExtendPaddleSize UMETA(Displayname ="Buff_ExtendPaddleSize"),
	ET_AdditionalBalls UMETA(Displayname ="Buff_AdditionalBalls"),
	ET_IncreaseMainBallSize UMETA(Displayname ="Buff_IncreaseMainBallSize"),
	ET_DoublePoints UMETA(Displayname ="Buff_DoublePoints"),
	ET_Fireball UMETA(Displayname ="Buff_Fireball"),
	ET_AddLife UMETA(Displayname ="Buff_AddLife"),
	ET_ShrinkPaddleSize UMETA(Displayname ="Debuff_ShrinkPaddleSize"),
	ET_DecreasePaddleSpeed UMETA(Displayname ="Debuff_DecreasePaddleSpeed"),
	ET_IncreaseBallSpeed UMETA(Displayname ="Debuff_IncreaseBallSpeed"),
	ET_ShrinkBallSize UMETA(Displayname ="Debuff_ShrinkBallSize"),
	ET_None UMETA(Displayname ="None_None")
};
