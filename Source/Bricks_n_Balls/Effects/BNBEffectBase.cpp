// Fill out your copyright notice in the Description page of Project Settings.

#include "BNBEffectBase.h"
#include "Kismet/GameplayStatics.h"
#include "Bricks_n_Balls/Player/BNBPawn.h"

// Sets default values
ABNBEffectBase::ABNBEffectBase()
{
	PlayerPawn = nullptr;
	FallSpeed = 20.f;
	FallDirection = FVector(-10.f, 0.f, 0.f);
}

// Called when the game starts or when spawned
void ABNBEffectBase::BeginPlay()
{
	Super::BeginPlay();
	
	PlayerPawn = Cast<ABNBPawn>(UGameplayStatics::GetPlayerPawn(this, 0));
}

void ABNBEffectBase::HandleOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
	UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	Super::HandleOverlap(OverlappedComponent, OtherActor, OtherComp, OtherBodyIndex, bFromSweep, SweepResult);
	
	if (PlayerPawn)
	{
		if (PlayerPawn == OtherActor)
		{
			ApplyEffect(PlayerPawn);
		}
	}
}

void ABNBEffectBase::ApplyEffect(ABNBPawn* Pawn)
{
	if (Pawn)
	{
		Pawn -> CallApplyEffect(EffectData);
		SetActive(this, false);
	}
}

// Called every frame
void ABNBEffectBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	Movement(DeltaTime);
}

void ABNBEffectBase::Movement(const float& DeltaTime)
{
	if (GetIsEnabled())
	{
		const FVector DeltaLocation = (FallSpeed * FallDirection) * DeltaTime;

		AddActorWorldOffset(DeltaLocation);
	}
}

void ABNBEffectBase::Deactivate()
{
	SetActive(this, false);	
}

void ABNBEffectBase::SetActive(AActor* Actor, const bool& Value)
{
	Super::SetActive(Actor, Value);
}

