// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Bricks_n_Balls/Objects/BNBDynamicObject.h"
#include "Bricks_n_Balls/Structs/BNBEffectData.h"
#include "BNBEffectBase.generated.h"

class ABNBPawn;
class UBoxComponent;
class UStaticMeshComponent;

UCLASS()
class BRICKS_N_BALLS_API ABNBEffectBase : public ABNBDynamicObject
{
	GENERATED_BODY()

public:	
	// Sets default values for this actor's properties
	ABNBEffectBase();
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	void Deactivate();
	virtual void SetActive(AActor* Actor, const bool& Value) override;
	
private:
	virtual void Movement(const float& DeltaTime) override;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	virtual void ApplyEffect(ABNBPawn* Pawn);
	
	virtual void HandleOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp,
		int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult) override;

public:	
private:
	UPROPERTY(EditDefaultsOnly)
	float FallSpeed;
	UPROPERTY(EditDefaultsOnly)
	FVector FallDirection;
protected:
	UPROPERTY()
	ABNBPawn* PlayerPawn;
	UPROPERTY(EditDefaultsOnly)
	FBNBEffectData EffectData;
};